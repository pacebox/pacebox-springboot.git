package tech.mhuang.pacebox.springboot.protocol;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 排序
 *
 * @author mhuang
 * @since 2020.0.2.3
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SortField implements Serializable {

    public static final String ORDER_ASC = "asc";
    public static final String ORDER_DESC = "desc";

    /**
     * 字段名
     */
    private String field;

    /**
     * 顺序。一般是asc desc
     */
    private String order;
}
