package tech.mhuang.pacebox.springboot.protocol.data;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collections;
import java.util.List;

/**
 * 分页应答
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageVO<T> {

    /**
     * 总行数
     */
    @Schema(description = "总条数")
    private Integer totalSize;

    @Schema(description = "最后id数（用于大数据进行分页查询时）")
    private String lastId;

    /**
     * 业务数据 <LIST>
     */
    @Schema(description = "业务数据")
    private List<T> result = Collections.emptyList();
}
