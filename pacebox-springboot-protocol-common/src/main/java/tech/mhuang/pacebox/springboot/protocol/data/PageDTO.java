package tech.mhuang.pacebox.springboot.protocol.data;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 分页DTO
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
public class PageDTO {

    @Schema(description = "每页行数,默认10行", example = "10")
    Integer rows = 10;

    @Schema(description = "开始页数,默认从第一页开始",example = "1")
    Integer start = 1;

    @Schema(description = "最后id数")
    private String lastId;
}
