package tech.mhuang.pacebox.springboot.protocol.data;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 通用树形结构
 *
 * @author mhuang
 * @since 1.1.2
 */
@Data
@EqualsAndHashCode(of = {"id"})
public class BaseTreeNode<T extends BaseTreeNode<T>> {

    @Schema(description = "当前节点id")
    private String id;

    @Schema(description = "当前节点名称")
    private String name;

    @Schema(description = "父节点id")
    private String parentId;

    @Schema(description = "父节点名称")
    private String parentName;

    @Schema(description = "子节点列表")
    private List<T> children;

    @Schema(description = "排序值")
    private Integer sorted;
}
