package tech.mhuang.pacebox.springboot.protocol;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;

/**
 * 保存记录
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
public class InsertInto<Id> implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    public static final String ADD = "1";
    public static final String UPDATE = "2";
    public static final String DELETE = "3";

    private Id id;

    private String reqNo;

    private String status;

    private String userId;

    private Date opDate;
}
