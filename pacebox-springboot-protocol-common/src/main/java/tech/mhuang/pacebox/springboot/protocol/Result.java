package tech.mhuang.pacebox.springboot.protocol;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 通用应答
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Result<T> {

    public static final int SUCCESS = 200;
    public static final int SYS_FAILD = 500;
    public static final int SYS_RESULT_FAILD = 202;
    public static final int TOKEN_EXPIRED = 401;
    public static final int SERVICE_UNAVAILABLE = 503;

    public static final String SUCCESS_MSG = "操作成功";
    public static final String FAILD_MSG = "操作失败";
    public static final String TOKEN_EXPIRED_MSG = "当前用户登录已过期,请重新登录";
    public static final String TOKEN_IS_VALID_MSG = "非法访问,请重新登录";
    public static final String SERVICE_UNAVAILABLE_MSG = "服务不可用";

    @Schema(description  = "返回状态")
    private int code;

    @Schema(description = "提示出错的字段")
    @JsonInclude(Include.NON_NULL)
    private String field;

    @Schema(description = "返回信息")
    private String message;

    @Schema(description = "返回的对象")
    @JsonInclude(Include.NON_NULL)
    private T data;

    @Schema(description = "异常信息")
    @JsonInclude(Include.NON_NULL)
    private Object exceptionMsg;

    public Result(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public Result(int code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public Result<T> success(T data) {
        this.code = SUCCESS;
        this.message = SUCCESS_MSG;
        this.data = data;
        return this;
    }

    public Result(T data) {
        this.data = data;
    }

    /**
     * 状态应答
     * @param code 状态码
     * @param message 消息
     * @return 结果集
     * @since 2021.0.5.0
     */
    public static Result status(int code, String message) {
        return new Result(code, message);
    }

    public static <T> Result<T> ok() {
        return new Result<>(SUCCESS, SUCCESS_MSG);
    }

    public static <T> Result<T> faild() {
        return new Result<>(SYS_FAILD, FAILD_MSG);
    }

    public static <T> Result<T> faild(String msg) {
        return new Result<>(SYS_FAILD, msg);
    }

    public static <T> Result<T> ok(T data) {
        return new Result<>(SUCCESS, SUCCESS_MSG, data);
    }

    public static <T> Result<T> tokenValid() {
        return new Result<>(TOKEN_EXPIRED, TOKEN_IS_VALID_MSG);
    }

    @JsonIgnore
    public boolean isOk() {
        return isOk(SUCCESS);
    }

    @JsonIgnore
    public boolean isOk(int successCode) {
        return code == successCode;
    }

    @JsonIgnore
    public boolean isFaild() {
        return !isOk();
    }

    @JsonIgnore
    public boolean isFaild(int successCode) {
        return !isOk(successCode);
    }
}
