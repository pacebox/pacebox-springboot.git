package tech.mhuang.pacebox.springboot.protocol;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 通用API忽略
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
public class BaseApiIgnore {

    @Schema(description = "用户id", hidden = true)
    private String userId;
}
