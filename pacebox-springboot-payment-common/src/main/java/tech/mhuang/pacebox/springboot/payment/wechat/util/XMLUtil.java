package tech.mhuang.pacebox.springboot.payment.wechat.util;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import tech.mhuang.pacebox.core.charset.CharsetType;
import tech.mhuang.pacebox.core.util.CollectionUtil;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * xml工具类
 *
 * @author mhuang
 * @since 1.0.0
 */
public class XMLUtil {

    public static Map doXmlParse(String strxml) throws JDOMException, IOException {
        strxml = strxml.replaceFirst("encoding=\".*\"", "encoding=\"UTF-8\"");

        if (strxml.isEmpty()) {
            return null;
        }

        InputStream in = new ByteArrayInputStream(strxml.getBytes(CharsetType.UTF_8_NAME));
        SAXBuilder builder = new SAXBuilder();
        Document doc = builder.build(in);
        Element root = doc.getRootElement();
        List<Element> list = root.getChildren();
        Map m = CollectionUtil.capacity(HashMap.class, list.size());
        for (Element o : list) {
            String k = o.getName();
            String v;
            List<Element> children = o.getChildren();
            if (children.isEmpty()) {
                v = o.getTextNormalize();
            } else {
                v = XMLUtil.getChildrenText(children);
            }

            m.put(k, v);
        }

        // 关闭流
        in.close();

        return m;
    }

    /**
     * 获取子结点的xml
     *
     * @param children child
     * @return String xmlString
     */
    @SuppressWarnings({"rawtypes"})
    public static String getChildrenText(List children) {
        StringBuilder sb = new StringBuilder();
        if (!children.isEmpty()) {
            for (Object child : children) {
                Element e = (Element) child;
                String name = e.getName();
                String value = e.getTextNormalize();
                List list = e.getChildren();
                sb.append("<").append(name).append(">");
                if (!list.isEmpty()) {
                    sb.append(XMLUtil.getChildrenText(list));
                }
                sb.append(value);
                sb.append("</").append(name).append(">");
            }
        }

        return sb.toString();
    }
}
