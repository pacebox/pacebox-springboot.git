package tech.mhuang.pacebox.springboot.payment.dto;

import lombok.Data;

/**
 * 退款实体
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
public class WechatRefundDTO {

    /**
     * 应用id
     */
    private String appId;

    /**
     * 支付密匙
     */
    private String apiKey;

    /**
     * 商户id
     */
    private String mchId;

    /**
     * 随机字符串
     */
    private String nonceStr;

    /**
     * 订单号(微信订单号与商户订单号二选一)
     */
    private String tradeNo;

    /**
     * 退款单号
     */
    private String outRefundNo;

    /**
     * 订单金额
     */
    private String totalFee;

    /**
     * 退款金额
     */
    private String refundFee;

    /**
     * 代理的主机
     */
    private String proxyHost;

    /**
     * 代理的端口
     */
    private Integer proxyPort;

    /**
     * 证书地址
     */
    private String certPath;

    //////////一下是非必填项/////
    /**
     * 退款货币类型，需与支付一致，或者不填。符合ISO 4217标准的三位字母代码，默认人民币：CNY，其他值列表详见货币类型
     */
    private String feeType;

    /**
     * 退款原因（80）
     */
    private String refundDesc;


    /**
     * 通知URL
     */
    private String notifyUrl;
}