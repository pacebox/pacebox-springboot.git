package tech.mhuang.pacebox.springboot.payment.wechat.util;

import tech.mhuang.pacebox.core.date.DatePattern;

import java.time.LocalTime;
import java.util.SortedMap;

/**
 * 微信配置URL
 *
 * @author mhuang
 * @since 1.0.0
 */
public class WechatConfigUtil {

    public final static String UNIFIED_ORDER_URL = "https://api.mch.weixin.qq.com/pay/unifiedorder";

    public final static String REFUND_URL = "https://api.mch.weixin.qq.com/secapi/pay/refund";

    /**
     * 基础参数
     *
     * @param packageParams 存放的map
     * @param appId         appid
     * @param mchId         商户id
     */
    public static void commonParams(SortedMap<Object, Object> packageParams, String appId, String mchId) {
        // 账号信息
        // 生成随机字符串
        String strTime = LocalTime.now().format(DatePattern.PURE_TIME_FORMAT);
        String strRandom = PayCommonUtil.buildRandom(4) + "";
        String nonceStr = strTime + strRandom;
        // 公众账号ID
        packageParams.put("appid", appId);
        // 商户号
        packageParams.put("mch_id", mchId);
        // 随机字符串
        packageParams.put("nonce_str", nonceStr);
    }
}