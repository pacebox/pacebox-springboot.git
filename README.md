<p align="center">
pacebox-springboot 基于pacebox & springboot 融合的工具包
</p>
<p align="center">
-- 主页：<a href="http://mhuang.tech/pacebox-springboot">http://mhuang.tech/pacebox-springboot</a>  --
</p>
<p align="center">
    -- QQ群①:<a target="_blank" href="//shang.qq.com/wpa/qunwpa?idkey=6703688b236038908f6c89b732758d00104b336a3a97bb511048d6fdc674ca01"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="pacebox官方交流群①" title="pacebox官方交流群①"></a>
</p>
---------------------------------------------------------------------------------------------------------------------------------------------------------

## 简介

pacebox-springboot 是一个基于pacebox & springboot融合的工具包、将pacebox以及springboot无缝接入

## 结构

```
    - pacebox.springboot-auth-common     权限拦截器工具包
    - pacebox.springboot-autoconfiguation  自动注入工具包（根据配置开关进行自行使用）
        - auth （默认提供的鉴权校验方式）
        - datasecure（数据保护）
        - elasticsearch （基于pacebox-elasticsearch)提供的springboot支持
        - exception （封装全局异常使用方式）
        - jackson （基于jackson的扩展支持）
        - jwt (基于pacebox-jwt）提供的springboot支持
        - kafka （基于pacebox-kafka)提供的springboot支持
        - kaptcha （验证码的封装）
        - oss 文件配置（系统支持腾讯COS、阿里OSS、百度BOS）
        - redis （redis集成）
        - rest （RestTemplate扩展封装）
        - sms 短信配置（支持腾讯、阿里、百度）短信接口
        - swagger 在线接口、基于knife4j封装（支持gateway、servlet配置）
        - task （任务扩展简单封装）
        - trace 链路追踪 基于opentracing实现、支持restTemplate、webClient、servlet、mybatis、短信埋点
        - validate （validation校验扩展）
        - wechat 微信相关服务开关
    - pacebox.springboot-core-common      核心处理工具包
    - pacebox.springboot-payment-common   支付工具包（支持微信、支付宝支付退款）
    - pacebox.springboot-protocol-common  协议工具包
    - pacebox.springboot-redis-common     基于redisTemplate简单封装工具包
    - pacebox.springboot-redis-kafka-middle-common  kafka消费数据通过aop写入redis的工具包
    - pacebox.springboot-starter-common   启动依赖工具包
    - pacebox.springboot-wechat-common    微信封装工具包（包含小程序和微信公众号支持）
```

## 安装

### MAVEN

在pom.xml中加入

```
    <dependency>
        <groupId>tech.mhuang.pacebox</groupId>
        <artifactId>pacebox-springboot</artifactId>
        <version>${laster.version}</version>
    </dependency>
```

### 非MAVEN

下载任意链接

- [Maven中央库1](https://repo1.maven.org/maven2/tech/mhuang/pacebox/pacebox-springboot/)
- [Maven中央库2](http://repo2.maven.org/maven2/tech/mhuang/pacebox/pacebox-springboot/)

## demo案例

### SpringBoot

[点击访问源码](http://gitee.com/pacebox/inter-boot-demo)

### SpringCloud

[点击访问源码](http://gitee.com/pacebox/inter-micro-demo)

### 代码生成器

[点击访问源码](http://gitee.com/pacebox/inter-boot-fastdfs)
> 注意
> pacebox只支持jdk1.8以上的版本