package tech.mhuang.pacebox.springboot.auth;

import io.jsonwebtoken.ExpiredJwtException;
import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tech.mhuang.pacebox.core.exception.BusinessException;
import tech.mhuang.pacebox.core.util.ObjectUtil;
import tech.mhuang.pacebox.core.util.StringUtil;
import tech.mhuang.pacebox.jwt.admin.JwtFramework;
import tech.mhuang.pacebox.jwt.admin.bean.Jwt;
import tech.mhuang.pacebox.springboot.core.constans.Global;
import tech.mhuang.pacebox.springboot.core.jackson.JsonUtil;
import tech.mhuang.pacebox.springboot.core.local.GlobalHeaderThreadLocal;
import tech.mhuang.pacebox.springboot.core.servlet.WebServletRequest;
import tech.mhuang.pacebox.springboot.core.spring.start.SpringContextHolder;
import tech.mhuang.pacebox.springboot.core.spring.util.DataUtil;
import tech.mhuang.pacebox.springboot.core.spring.util.IpUtil;
import tech.mhuang.pacebox.springboot.core.spring.util.SpringServletUtil;
import tech.mhuang.pacebox.springboot.protocol.GlobalHeader;
import tech.mhuang.pacebox.springboot.protocol.Result;


import java.io.IOException;
import java.util.Enumeration;
import java.util.Map;

/**
 * 通用权限处理
 *
 * @author mhuang
 * @since 1.0.0
 */
public class AuthFilter implements Filter {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Setter
    @Getter
    private String defaultAuthTypeValue;


    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (req instanceof WebServletRequest request) {
            try {
                logger.debug("请求的URL:{},请求类型:{}", request.getRequestURL().toString(), request.getMethod());
                GlobalHeader globalHeader = new GlobalHeader();
                globalHeader.setIp(IpUtil.getIp(request));
                globalHeader.setSource(request.getHeader(Global.SOURCE));
                globalHeader.setVersion(request.getHeader(Global.VERSION));
                globalHeader.setTenantId(request.getHeader(Global.TENANT_ID));
                globalHeader.setLang(request.getHeader(Global.LANGUAGE));
                Enumeration<String> headerNames = request.getHeaderNames();
                while (headerNames.hasMoreElements()) {
                    String headerName = headerNames.nextElement();
                    if (!Global.IGNORE_HEADER.contains(headerName)) {
                        globalHeader.getExtraMap().put(headerName, request.getHeader(headerName).replaceAll("\"","'"));
                    }
                }
                JwtFramework jwtFramework = SpringContextHolder.getBean(JwtFramework.class);
                String authType = request.getHeader(Global.AUTH_TYPE);
                String auth = null, headerName = null;
                if (StringUtil.isEmpty(authType)) {
                    authType = defaultAuthTypeValue;
                    if (StringUtil.isEmpty(authType)) {
                        chain.doFilter(request, response);
                        return;
                    }
                }
                Jwt.JwtBean jwtBean = jwtFramework.getJwt().getBeanMap().get(authType);
                if (ObjectUtil.isNotEmpty(jwtBean)) {
                    auth = request.getHeader(jwtBean.getType());
                    headerName = jwtBean.getHeaderName();
                }
                String BASIC = "Basic";
                if (StringUtil.isBlank(auth)) {
                    GlobalHeaderThreadLocal.set(globalHeader);
                    request.putHeader(Global.GLOBAL_HEADER, JsonUtil.toString(globalHeader));
                } else if (StringUtil.indexOf(auth, BASIC) == 0) {
                    GlobalHeaderThreadLocal.set(globalHeader);
                    request.putHeader(Global.GLOBAL_HEADER, JsonUtil.toString(globalHeader));
                } else if (StringUtil.length(auth) > headerName.length()) {
                    logger.debug("当前调用的token:{}", auth);
                    String token = StringUtil.substringAfter(auth, headerName);
                    try {
                        Map<String, Object> claimMap = jwtFramework.getProducer(authType).parse(token);
                        if (claimMap != null) {
                            globalHeader.setToken(token.trim());
                            globalHeader.setType((String) claimMap.get(Global.TYPE));
                            globalHeader.setCompanyId((String) claimMap.get(Global.COMPANY_ID));
                            globalHeader.setUserId((String) claimMap.get(Global.USER_ID));
                            GlobalHeaderThreadLocal.set(globalHeader);
                            request.putHeader(Global.GLOBAL_HEADER, JsonUtil.toString(globalHeader));
                        } else {
                            throw new BusinessException(Result.TOKEN_EXPIRED, Result.TOKEN_IS_VALID_MSG);
                        }
                    } catch (ExpiredJwtException e) {
                        logger.error("token已过期", e);
                        throw new BusinessException(Result.TOKEN_EXPIRED, Result.TOKEN_EXPIRED_MSG);
                    } catch (Exception e) {
                        logger.error("token异常", e);
                        throw new BusinessException(Result.TOKEN_EXPIRED, Result.TOKEN_IS_VALID_MSG);
                    }
                } else {
                    logger.error("token:{}无效，长度不一致", auth);
                    throw new BusinessException(Result.TOKEN_EXPIRED, Result.TOKEN_IS_VALID_MSG);
                }
                chain.doFilter(request, response);
            } catch (BusinessException e) {
                setResponseCors(response, e);
            }
        }
    }

    private void setResponseCors(ServletResponse response, BusinessException e) throws IOException {
        HttpServletResponse errResponse = (HttpServletResponse) response;
        //解决跨域访问报错
        errResponse.setHeader("Access-Control-Allow-Origin", "*");
        errResponse.setHeader("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS, DELETE");
        //设置过期时间
        errResponse.setHeader("Access-Control-Max-Age", "3600");
        errResponse.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, client_id, uuid, Authorization");
        // 支持HTTP 1.1.
        errResponse.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        errResponse.setHeader("Pragma", "no-cache");
        errResponse.setContentType("application/json;charset=utf-8");
        Result<?> result = DataUtil.copyTo(e, Result.class);
        SpringServletUtil.write(errResponse, JsonUtil.toString(result));
    }

    @Override
    public void destroy() {
        GlobalHeaderThreadLocal.remove();
    }
}
