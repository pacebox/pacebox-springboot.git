package tech.mhuang.pacebox.springboot.rediskafkamiddle;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import tech.mhuang.pacebox.core.util.StringUtil;
import tech.mhuang.pacebox.kafka.global.exception.JkafkaException;
import tech.mhuang.pacebox.kafka.producer.bean.KafkaMsg;
import tech.mhuang.pacebox.springboot.core.jackson.JsonUtil;
import tech.mhuang.pacebox.springboot.core.util.SpringAnnotationUtil;
import tech.mhuang.pacebox.springboot.redis.commands.RedisExtCommands;
import tech.mhuang.pacebox.springboot.redis.lock.DistributedLockHandler;
import tech.mhuang.pacebox.springboot.redis.lock.Lock;
import tech.mhuang.pacebox.springboot.rediskafkamiddle.annaotion.RedisKafka;

/**
 * 拦截Kafka进行处理
 *
 * @author mhuang
 * @since 1.0.0
 */
@Component
@Aspect
@Order(100)
public class RedisKafkaAspect {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final RedisExtCommands redisExtCommands;

    private final DistributedLockHandler distributedLockHandler;

    /**
     * redis存放的DB
     */
    @Value("${redisKafkaDataBase:0}")
    private int redisKafkaDataBase;

    public RedisKafkaAspect(RedisExtCommands redisExtCommands, DistributedLockHandler distributedLockHandler) {
        this.redisExtCommands = redisExtCommands;
        this.distributedLockHandler = distributedLockHandler;
    }

    @Pointcut("@annotation(tech.mhuang.pacebox.springboot.rediskafkamiddle.annaotion.RedisKafka)")
    private void kafkaMsgProcess() {}

    @Before("kafkaMsgProcess()")
    public void doBefore(JoinPoint joinPoint) {
        logger.debug("---正在拦截---kafka---");
        Object[] obj = joinPoint.getArgs();
        KafkaMsg kafkaMsg = (KafkaMsg) obj[0];
        Lock lock = new Lock();
        String CONSUMER = "-consumer";
        try {
            RedisKafka rediksKafka = SpringAnnotationUtil.getAnnotation(joinPoint, RedisKafka.class);
            Boolean notRepeat = rediksKafka.notRepeat();
            String LOCK = "-lock-";
            lock.setName(kafkaMsg.getTopic() + LOCK + kafkaMsg.getOffset());
            lock.setValue(kafkaMsg.getMsg().toString());
            if (distributedLockHandler.tryLock(lock, notRepeat)) {
                String value = redisExtCommands.hget(redisKafkaDataBase, kafkaMsg.getTopic() + CONSUMER, "" + kafkaMsg.getOffset());
                if (StringUtil.isEmpty(value)) {
                    redisExtCommands.hset(redisKafkaDataBase, kafkaMsg.getTopic() + CONSUMER, "" + kafkaMsg.getOffset(), kafkaMsg.getMsg());
                } else {
                    logger.error("数据为：{}", JsonUtil.toString(kafkaMsg));
                    throw new JkafkaException("kafka这条消息已经处理过了！");
                }
            }
        } catch (Exception e) {
            if (!(e instanceof JkafkaException)) {
                redisExtCommands.hdel(redisKafkaDataBase, kafkaMsg.getTopic() + CONSUMER, String.valueOf(kafkaMsg.getOffset()));
            }
        } finally {
            distributedLockHandler.releaseLock(lock);
        }
    }
}
