package tech.mhuang.pacebox.springboot.wechat.wechat.common.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import tech.mhuang.pacebox.springboot.wechat.wechat.common.consts.WechatConsts;

import java.io.Serializable;
import java.util.List;

/**
 * 微信授权应答
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
public class WechatAuthAllReturn implements Serializable {

    @JsonProperty(value = WechatConsts.OPENID)
    private String openid;

    @JsonProperty(value = WechatConsts.NICKNAME)
    private String nickname;

    @JsonProperty(value = WechatConsts.SEX)
    private String sex;

    @JsonProperty(value = WechatConsts.PROVINCE)
    private String province;

    @JsonProperty(value = WechatConsts.CITY)
    private String city;

    @JsonProperty(value = WechatConsts.COUNTRY)
    private String country;

    @JsonProperty(value = WechatConsts.HEADIMGURL)
    private String headimgurl;

    @JsonProperty(value = WechatConsts.PRIVILEGE)
    private List<String> privilege;

    @JsonProperty(value = WechatConsts.UNIONID)
    private String unionid;
}
