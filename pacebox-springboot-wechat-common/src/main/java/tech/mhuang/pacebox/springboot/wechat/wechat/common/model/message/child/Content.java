package tech.mhuang.pacebox.springboot.wechat.wechat.common.model.message.child;

import lombok.Data;

/**
 * 内容
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
public class Content {

    String content;

    public Content() {

    }
}
