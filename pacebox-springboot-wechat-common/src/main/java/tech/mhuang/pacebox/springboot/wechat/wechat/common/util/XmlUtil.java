package tech.mhuang.pacebox.springboot.wechat.wechat.common.util;

import org.dom4j.Document;
import org.dom4j.Element;
import tech.mhuang.pacebox.core.util.CollectionUtil;
import tech.mhuang.pacebox.core.util.StringUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author mhuang
 */
public class XmlUtil {

    public static Map<String, Object> dom2Map(Document doc) {

        if (doc == null) {
            return CollectionUtil.capacity(HashMap.class, 4);
        }

        Element root = doc.getRootElement();
        Iterator<Element> iterator = root.elementIterator();
        Map<String, Object> map = CollectionUtil.capacity(HashMap.class, 20);
        while (iterator.hasNext()) {
            Element e = iterator.next();
            List<Element> list = e.elements();
            if (!list.isEmpty()) {
                map.put(e.getName(), dom2Map(e));
            } else {
                map.put(e.getName(), e.getText());
            }

        }
        return map;
    }


    public static Map dom2Map(Element e) {
        List<Element> list = e.elements();
        Map map = CollectionUtil.capacity(HashMap.class, list.size());
        if (!list.isEmpty()) {
            for (Element o : list) {
                List mapList;

                if (!o.elements().isEmpty()) {
                    Map m = dom2Map(o);
                    if (map.get(o.getName()) != null) {
                        Object obj = map.get(o.getName());
                        if (StringUtil.equals(obj.getClass().getName(), "java.util.ArrayList")) {
                            mapList = (List) obj;
                            mapList.add(m);
                        } else {
                            mapList = new ArrayList<>();
                            mapList.add(obj);
                            mapList.add(m);
                        }
                        map.put(o.getName(), mapList);
                    } else {
                        map.put(o.getName(), m);
                    }
                } else {
                    if (map.get(o.getName()) != null) {
                        Object obj = map.get(o.getName());
                        if (StringUtil.equals(obj.getClass().getName(), "java.util.ArrayList")) {
                            mapList = (List) obj;
                            mapList.add(o.getText());
                        } else {
                            mapList = new ArrayList();
                            mapList.add(obj);
                            mapList.add(o.getText());
                        }
                        map.put(o.getName(), mapList);
                    } else {
                        map.put(o.getName(), o.getText());
                    }
                }
            }
        } else {
            map.put(e.getName(), e.getText());
        }

        return map;
    }
}
