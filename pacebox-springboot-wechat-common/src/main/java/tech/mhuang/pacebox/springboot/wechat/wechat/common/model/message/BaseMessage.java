package tech.mhuang.pacebox.springboot.wechat.wechat.common.model.message;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import tech.mhuang.pacebox.core.timer.SystemClock;
import tech.mhuang.pacebox.springboot.wechat.wechat.common.consts.WechatConsts;

import java.io.Serial;
import java.io.Serializable;

/**
 * 基础消息配置
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
public abstract class BaseMessage implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    @JsonProperty(WechatConsts.TOUSER)
    private String toUserName;

    @JsonIgnore
    private String fromUserName;

    @JsonIgnore
    private Long createTime;

    @JsonProperty(WechatConsts.MSGTYPE)
    private String msgType = WechatConsts.NULL_STR;

    public BaseMessage() {

    }

    public BaseMessage(String tuser) {
        setToUserName(tuser);
    }

    public BaseMessage(String toUserName, String fromUserName) {
        setToUserName(toUserName);
        setFromUserName(fromUserName);
        setCreateTime(SystemClock.now());
    }
}
