package tech.mhuang.pacebox.springboot.wechat.wechat.common.model.template;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * 小程序模板管理
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
public class MiniTemplate implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 发送的openid
     */
    private String touser;

    /**
     * 模板id
     */
    @JsonProperty("template_id")
    private String templateId = "";

    /**
     * 跳转的页面
     */
    private String page;

    /**
     * 表单场景
     */
    @JsonProperty("form_id")
    private String formId;


    /**
     * 高亮文本
     */
    @JsonProperty("emphasis_keyword")
    private String emphasisKeyword;

    /**
     * 默认颜色
     */
    private String color;

    private Map<String, Map<String, String>> data = new HashMap<>();

    private final static String DEFAULT_COLOR = "#173177";
    private final static String FIRST = "first";
    private final static String KEYWORD = "keyword";
    private final static String VALUE = "value";
    private final static String COLOR = "color";
    private final static String REMARK = "remark";

    public void addKeyword(Integer index, String value) {
        addKeyword(index, value, DEFAULT_COLOR);
    }

    public void addOther(String name, String value) {
        addOther(name, value, DEFAULT_COLOR);
    }

    public void addOther(String name, String value, String color) {
        addDataMap(name, value, color);
    }

    public void addKeyword(Integer index, String value, String color) {
        addDataMap(KEYWORD + index, value, color);
    }

    public void addRemark(String value) {
        addRemark(value, DEFAULT_COLOR);
    }

    public void addRemark(String value, String color) {
        addDataMap(REMARK, value, color);
    }

    public void addFirst(String value) {
        addFirst(value, DEFAULT_COLOR);
    }

    public void addFirst(String value, String color) {
        addDataMap(FIRST, value, color);
    }

    private void addDataMap(String type, String value, String color) {
        Map<String, String> map = new HashMap<>(4);
        map.put(VALUE, value);
        map.put(COLOR, color);
        data.put(type, map);
    }
}
