package tech.mhuang.pacebox.springboot.wechat.wechat.common.model.message.child;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import tech.mhuang.pacebox.springboot.wechat.wechat.common.consts.WechatConsts;

import java.io.Serial;
import java.io.Serializable;


/**
 * 应答的音乐消息
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Music extends BaseChildMessage implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    @JsonProperty(WechatConsts.MUSICURL)
    private String musicUrl;

    @JsonProperty(WechatConsts.HQMUSICURL)
    private String hqMusicUrl;

    @JsonProperty(WechatConsts.THUMB_MEDIA_URL)
    private String thumbMediaId;

    public static Music setMusicMessage(String title, String descption, String musicUrl, String hqMusicUrl, String thumbMediaId) {
        Music music = new Music();
        music.setTitle(title);
        music.setDescption(descption);
        music.setMusicUrl(musicUrl);
        music.setHqMusicUrl(hqMusicUrl);
        music.setThumbMediaId(thumbMediaId);
        return music;
    }
}
