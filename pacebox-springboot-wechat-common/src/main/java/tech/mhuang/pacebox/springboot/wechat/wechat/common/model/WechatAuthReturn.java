package tech.mhuang.pacebox.springboot.wechat.wechat.common.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import tech.mhuang.pacebox.springboot.wechat.wechat.common.consts.WechatConsts;

import java.io.Serializable;

/**
 * 微信授权简单回答
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
public class WechatAuthReturn implements Serializable {

    @JsonProperty(value = WechatConsts.SCOPE)
    private String scope;

    @JsonProperty(value = WechatConsts.OPENID)
    private String openid;

    @JsonProperty(value = WechatConsts.ACCESS_TOKEN_FIELD)
    private String accessToken;

    @JsonProperty(value = WechatConsts.REFRESH_TOKEN)
    private String refreshToken;

    @JsonProperty(value = WechatConsts.EXPIRES_IN)
    private String expiresIn;
}
