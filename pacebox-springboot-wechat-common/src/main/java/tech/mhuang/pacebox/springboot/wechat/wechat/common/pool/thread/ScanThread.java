package tech.mhuang.pacebox.springboot.wechat.wechat.common.pool.thread;

import tech.mhuang.pacebox.springboot.wechat.wechat.common.pool.service.ExecuteService;

/**
 * 扫码事件
 *
 * @author mhuang
 * @since 1.0.0
 */
public class ScanThread extends BaseThread {


    /**
     * 关注带事件
     */
    private final String eventKey;

    public ScanThread(String openId, String eventKey, ExecuteService weChatService) {
        super(openId, weChatService);
        this.eventKey = eventKey;
    }

    @Override
    public void run() {
        synchronized (openId) {
            weChatService.scan(openId, eventKey);
        }
    }

}
