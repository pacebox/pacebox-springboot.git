package tech.mhuang.pacebox.springboot.wechat.wechat.common.model.template;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import tech.mhuang.pacebox.springboot.wechat.wechat.common.consts.WechatConsts;

import java.io.Serializable;

/**
 * 微信模板应答
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TemplateReturn implements Serializable {

    @XStreamAlias(WechatConsts.ERROR_CODE)
    @JsonProperty(value = WechatConsts.ERROR_CODE)
    private String errorCode;

    @XStreamAlias(WechatConsts.ERROR_MSG)
    @JsonProperty(value = WechatConsts.ERROR_MSG)
    private String errorMsg;

    @XStreamAlias(WechatConsts.MSG_ID)
    @JsonProperty(value = WechatConsts.MSG_ID)
    private String msgId;
}