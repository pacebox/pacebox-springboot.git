package tech.mhuang.pacebox.springboot.wechat.wechat.common.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import tech.mhuang.pacebox.springboot.wechat.wechat.common.consts.WechatConsts;

import java.io.Serializable;

/**
 * 微信Token应答
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
public class WechatTokenReturn implements Serializable {

    @JsonProperty(value = WechatConsts.ERROR_CODE)
    private int errcode;

    @JsonProperty(value = WechatConsts.ERROR_MSG)
    private String errmsg;

    @JsonProperty(value = WechatConsts.ACCESS_TOKEN_FIELD)
    private String accessToken;

    @JsonProperty(value = WechatConsts.EXPIRES_IN)
    private String expiresIn;
}
