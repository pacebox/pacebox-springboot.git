package tech.mhuang.pacebox.springboot.wechat.wechat.common.model.ret;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;
import tech.mhuang.pacebox.springboot.wechat.wechat.common.consts.WechatConsts;

import java.io.Serial;
import java.io.Serializable;

/**
 * 微信应答
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
public class Return implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    @XStreamAlias(WechatConsts.RETURN_CODE)
    @JsonProperty(value = WechatConsts.RETURN_CODE)
    private String returnCode;

    @XStreamAlias(WechatConsts.RETURN_MSG)
    @JsonProperty(value = WechatConsts.RETURN_MSG)
    private String returnMsg;

    public Return() {

    }

    public Return(String returnCode, String returnMsg) {
        this.returnCode = returnCode;
        this.returnMsg = returnMsg;
    }
}
