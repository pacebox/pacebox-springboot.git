package tech.mhuang.pacebox.springboot.wechat.wechat.common.model.message;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import tech.mhuang.pacebox.springboot.wechat.wechat.common.consts.WechatConsts;
import tech.mhuang.pacebox.springboot.wechat.wechat.common.model.message.child.Article;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 图文响应消息
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ArticleResMessage extends BaseMessage implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    private Integer articleCount = 0;

    @JsonIgnore
    private List<Article> articles = new ArrayList<>();

    @JsonProperty(WechatConsts.NEWS)
    @XStreamOmitField
    private ArticleList articleList = new ArticleList();

    public ArticleResMessage() {
        setMsgType(WechatConsts.NEWS);
    }

    public ArticleResMessage(String toUserName, String fromUserName) {
        super(toUserName, fromUserName);
        setMsgType(WechatConsts.NEWS);
    }

    public void addArticle(Article article) {
        articles.add(article);
        articleCount = articles.size();
    }

    public void addArticle(String title, String descption, String picUrl, String url) {
        addArticle(Article.getArticle(title, descption, picUrl, url));
    }

    public void addJsonArticle(Article article) {
        articleList.articles.add(article);
        articleCount = articleList.articles.size();
    }

    public void addJsonArticle(String title, String descption, String picUrl, String url) {
        addJsonArticle(Article.getArticle(title, descption, picUrl, url));
    }

    @Data
    static
    class ArticleList {
        private List<Article> articles = new ArrayList<>();
    }
}
