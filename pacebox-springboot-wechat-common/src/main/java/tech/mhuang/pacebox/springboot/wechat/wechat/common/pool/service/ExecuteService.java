package tech.mhuang.pacebox.springboot.wechat.wechat.common.pool.service;

/**
 * 执行接口..可自行拓展
 *
 * @author mhuang
 * @since 1.0.0
 */
public interface ExecuteService {

    /**
     * 分享（老方法）
     *
     * @param usrId     用户id
     * @param status    状态
     * @param type      类型
     * @param shareName 分享名称
     * @param uuid      唯一码
     */
    @Deprecated
    void share(String usrId, String status, String type,
               String shareName, String uuid);

    /**
     * 关注
     *
     * @param openId openid
     * @param status 状态
     */
    void subscribe(String openId, String status);

    /**
     * 扫码
     *
     * @param openId   openid
     * @param eventKey key
     */
    void scan(String openId, String eventKey);

    /**
     * 关注扫码监听
     *
     * @param openId   openid
     * @param status   状态
     * @param eventKey key
     */
    void subscribeOtherEvent(String openId, String status, String eventKey);

    /**
     * 消息
     *
     * @param openId  openid
     * @param content 内容
     */
    void saveOpTextSend(String openId, String content);
}
