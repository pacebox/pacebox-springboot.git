package tech.mhuang.pacebox.springboot.wechat.wechat.common.consts;

/**
 * 微信常量存放
 *
 * @author mhuang
 * @since 1.0.0
 */
public class WechatConsts {

    public final static String SCENE = "scene";
    public final static String ACTION_NAME = "action_name";
    public final static String EXPRICE_SECONDS = "expire_seconds";
    public final static String ACTION_INFO = "action_info";
    public final static String SCENE_ID = "scene_id";
    public final static String SCENE_STR = "scene_str";
    public final static String VIEW = "view";
    public final static String MEDIA_ID = "media_id";
    public final static String VIEW_LIMITED = "view_limited";
    public final static String MINIPROGRAM = "miniprogram";
    public final static String SUB_BUTTON = "sub_button";
    public final static String BUTTON = "button";
    public final static String NEWS = "news";
    public final static String TOUSER = "touser";
    public final static String NULL_STR = "";
    public final static String MSGTYPE = "msgtype";
    public final static String NULL = null;
    public final static String IMAGE = "image";
    public final static String VOICE = "voice";
    public final static String VIDEO = "video";
    public final static String MUSIC = "music";
    public final static String TEXT = "text";
    public final static String ITEM = "item";
    public final static String PICURL = "picurl";
    public final static String MUSICURL = "musicurl";
    public final static String HQMUSICURL = "hqmusicurl";
    public final static String THUMB_MEDIA_URL = "thumb_media_id";
    public final static String XML = "xml";
    public final static String LINK = "link";
    public final static String CLASS = "class";
    public final static String OTHERMESSAGE = "otherMessage";
    public final static String RETURN_CODE = "return_code";
    public final static String RETURN_MSG = "return_msg";
    public final static String ERROR_CODE = "errcode";
    public final static String ERROR_MSG = "errmsg";
    public final static String MSG_ID = "msgid";

    /**
     * 获取Token
     */
    public final static String ACCESS_TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&secret=APP_SECRET&appid=APP_ID";

    /**
     * 创建菜单
     */
    public final static String CREATE_MENU_URL = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=ACCESS_TOKEN";

    /**
     * 获取用户URL
     */
    public final static String GETUSER_URL = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=ACCESS_TOKEN&openid=OPEN_ID&lang=zh_CN";

    /**
     * auth授权获取code
     */
    public final static String WECHAT_OAUTH_GETCODE_URL = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=APP_ID&redirect_uri=REDIRECT_URL&response_type=code&scope=snsapi_base&state=STATE&connect_redirect=CONNECT_REDIRECT#wechat_redirect";

    /**
     * 微信网页授权Oauth2.0
     */
    public final static String WECHAT_OAUTH_URL = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=APP_ID&secret=APP_SECRET&code=CODE&grant_type=GRANT_TYPE";

    public final static String USER_INFO_URL = "https://api.weixin.qq.com/sns/userinfo?access_token=ACCESS_TOKEN&openid=OPEN_ID&lang=zh_CN";
    /**
     * 客服消息主动推送接口
     */
    public final static String WECHAT_CUSTOMER_SEND_URL = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=ACCESS_TOKEN";
    /**
     * 二维码请求票据接口
     */
    public final static String WECHAT_QRCODE_CREATE_URL = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=TOKEN";
    /**
     * 换取二维码接口
     */
    public final static String WECHAT_QRCODE_SHOW_URL = "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=TICKET";
    /**
     * 微信jsapi token接口
     */
    public final static String WECHAT_JSAPI_TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=ACCESS_TOKEN&type=jsapi";
    /**
     * 微信下载多媒体接口
     */
    public final static String WECHAT_DOWNLOAD_MEDIA_URL = "http://file.api.weixin.qq.com/cgi-bin/media/get?access_token=ACCESS_TOKEN&media_id=MEDIA_ID";
    /**
     * 微信上传多媒体接口
     */
    public final static String WECHAT_UPLOAD_MEDIA_URL = "https://api.weixin.qq.com/cgi-bin/media/upload?access_token=ACCESS_TOKEN";
    /**
     * 模板消息行业设置
     */
    public final static String WECHAT_TEMPLATE_SETTING = "https://api.weixin.qq.com/cgi-bin/template/api_set_industry?access_token=ACCESS_TOKEN";
    /**
     * 模板消息添加模板
     */
    public final static String WECHAT_TEMPLATE_ADD = "https://api.weixin.qq.com/cgi-bin/template/api_add_template?access_token=ACCESS_TOKEN";
    /**
     * 模板消息获取模板
     */
    public final static String WECHAT_TEMPLATE_GET = "https://api.weixin.qq.com/cgi-bin/template/get_all_private_template?access_token=ACCESS_TOKEN";
    /**
     * 模板消息删除模板
     */
    public final static String WECHAT_TEMPLATE_DELETE = "https://api,weixin.qq.com/cgi-bin/template/del_private_template?access_token=ACCESS_TOKEN";
    /**
     * 模板发送URL
     */
    public final static String TEMPLATE_SEND_URL = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=ACCESS_TOKEN";

    public final static String ACCESS_TOKEN = "ACCESS_TOKEN";
    public static final String APP_ID = "APP_ID";
    public static final String REDIRECT_URL = "REDIRECT_URL";
    public static final String STATE = "STATE";
    public static final String CONNECT_REDIRECT = "CONNECT_REDIRECT";
    public static final String GRANT_TYPE = "GRANT_TYPE";
    public static final String EXPIRES_IN = "expires_in";
    public static final String APP_SECRET = "APP_SECRET";
    public static final String ACCESS_TOKEN_FIELD = "access_token";
    public static final String TICKET_FIELD = "ticket";
    public static final String OPENID = "openid";
    public static final String REFRESH_TOKEN = "refresh_token";
    public static final String SCOPE = "scope";
    public static final String CODE = "CODE";
    public static final String NICKNAME = "nickname";
    public static final String SEX = "sex";
    public static final String PROVINCE = "province";
    public static final String CITY = "city";
    public static final String COUNTRY = "country";
    public static final String HEADIMGURL = "headimgurl";
    public static final String PRIVILEGE = "privilege";
    public static final String UNIONID = "unionid";

    public static final String OPEN_ID = "OPEN_ID";
}

