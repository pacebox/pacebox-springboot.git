package tech.mhuang.pacebox.springboot.wechat.wechat.common.model.message;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import tech.mhuang.pacebox.springboot.wechat.wechat.common.consts.WechatConsts;
import tech.mhuang.pacebox.springboot.wechat.wechat.common.model.message.child.Content;

import java.io.Serial;


/**
 * 应答的文本消息
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class TextResMessage extends BaseMessage {

    @Serial
    private static final long serialVersionUID = 1L;

    @JsonIgnore
    private String content;

    @JsonProperty(WechatConsts.TEXT)
    private Content contentes;


    public TextResMessage() {
        setMsgType(WechatConsts.TEXT);
    }


    public TextResMessage(String toUserName, String fromUserName) {
        super(toUserName, fromUserName);
        setMsgType(WechatConsts.TEXT);
    }

    public TextResMessage(String toUser) {
        super(toUser);
    }

    public void saveJson(String toUser, String content) {
        setToUserName(toUser);
        if (contentes == null) {
            contentes = new Content();
        }
        contentes.setContent(content);
    }
}
