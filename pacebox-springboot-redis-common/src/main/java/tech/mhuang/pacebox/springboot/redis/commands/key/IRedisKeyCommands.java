package tech.mhuang.pacebox.springboot.redis.commands.key;

import java.util.Set;

/**
 * redis key操作
 *
 * @author mhuang
 * @since 1.0.0
 */
public interface IRedisKeyCommands {

    /**
     * 设置过期时间（秒）
     *
     * @param key     key
     * @param seconds 秒数
     * @return boolean
     */
    Boolean expire(String key, long seconds);

    /**
     * 设置过期时间（秒）
     *
     * @param index   库
     * @param key     key
     * @param seconds 秒数
     * @return boolean
     */
    Boolean expire(int index, String key, long seconds);

    /**
     * 判断key是否存在
     *
     * @param key key
     * @return 存在返回True、不存在返回False
     */
    Boolean exists(String key);

    /**
     * 判断key是否存在
     *
     * @param index 库
     * @param key   key
     * @return 存在返回True、不存在返回False
     */
    Boolean exists(int index, String key);

    /**
     * 扫描获取最多的数据
     *
     * @param pattern 扫描的key
     * @param count   循环的固定大小
     * @param clazz   循环出的数据
     * @param <T>     循环的数据类型
     * @return 获取扫描结果
     */
    <T> Set<T> scan(String pattern, int count, Class<T> clazz);

    /**
     * 扫描获取最多的数据
     *
     * @param index   数据库
     * @param pattern 扫描的key
     * @param count   循环的固定大小
     * @param clazz   循环出的数据
     * @param <T>     循环的数据类型
     * @return 获取扫描结果
     */
    <T> Set<T> scan(int index, String pattern, int count, Class<T> clazz);
}
