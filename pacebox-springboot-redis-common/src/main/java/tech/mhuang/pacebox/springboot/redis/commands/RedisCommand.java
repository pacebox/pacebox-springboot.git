
package tech.mhuang.pacebox.springboot.redis.commands;

import org.springframework.data.redis.core.RedisTemplate;

/**
 * redis命令接口
 *
 * @author mhuang
 * @since 1.0.0
 */
public interface RedisCommand<T> {

    /**
     * 模板扩展调用
     *
     * @param template 模板
     * @return 结果
     */
    T executeCommand(RedisTemplate<?, ?> template);

}
