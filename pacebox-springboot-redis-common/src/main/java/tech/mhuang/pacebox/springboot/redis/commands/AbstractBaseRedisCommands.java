package tech.mhuang.pacebox.springboot.redis.commands;

import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.Cursor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ScanOptions;
import org.springframework.data.redis.serializer.RedisSerializer;
import tech.mhuang.pacebox.core.util.CollectionUtil;
import tech.mhuang.pacebox.springboot.core.jackson.JsonUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 通用抽象通用处理
 *
 * @author mhuang
 * @since 1.0.0
 */
public abstract class AbstractBaseRedisCommands implements IRedisExtCommands {

    @Setter
    RedisTemplate<String, ?> baseRedisTemplate;

    @Value("${spring.data.redis.database:0}")
    int defaultDbIndex;

    @Override
    public boolean hset(String key, String field, Object value) {
        return hset(defaultDbIndex, key, field, value);
    }

    @Override
    public <T>boolean hmset(String key, Map<String, T> params) {
        return hmset(defaultDbIndex, key, params);
    }

    @Override
    public Collection<String> hmget(String key, Collection<String> fields) {
        return hmget(defaultDbIndex, key, fields);
    }

    @Override
    public Map<String, String> hgetall(String key) {
        return hgetall(defaultDbIndex, key);
    }

    @Override
    public boolean zadd(String key, double score, Object value) {
        return zadd(defaultDbIndex, key, score, value);
    }

    @Override
    public boolean set(String key, Object value) {
        return set(defaultDbIndex, key, value);
    }

    @Override
    public boolean set(String key, Object value, long expireTime) {
        return set(defaultDbIndex, key, value, expireTime);
    }

    @Override
    public Long incr(String key) {
        return incr(defaultDbIndex, key);
    }

    @Override
    public String get(String key) {
        return get(defaultDbIndex, key);
    }

    @Override
    public boolean mset(Map<String, Object> map) {
        return mset(defaultDbIndex, map);
    }

    @Override
    public Collection<String> mget(Collection<String> keys) {
        return mget(defaultDbIndex, keys);
    }

    @Override
    public long del(String key) {
        return del(defaultDbIndex, key);
    }

    @Override
    public Long append(int index, String key, Object value) {
        return baseRedisTemplate.execute((RedisConnection connection) -> {
            connection.select(index);
            RedisSerializer<String> serializer = baseRedisTemplate.getStringSerializer();
            return connection.stringCommands().append(Objects.requireNonNull(serializer.serialize(key)), Objects.requireNonNull(serializer.serialize(value instanceof String str? str : toString(value))));
        });
    }


    private String toString(Object value){
        return JsonUtil.toString(value);
    }
    /**
     * 添加
     *
     * @param key   简
     * @param score 分数
     * @param value 值
     * @return boolean
     */
    @Override
    public boolean zadd(int index, String key, double score, Object value) {
        return Boolean.TRUE.equals(baseRedisTemplate.execute((RedisConnection connection) -> {
            connection.select(index);
            RedisSerializer<String> serializer = baseRedisTemplate.getStringSerializer();
            String val = value instanceof String str ? str : toString(value);
            return connection.zAdd(Objects.requireNonNull(serializer.serialize(key)), score, Objects.requireNonNull(serializer.serialize(val)));
        }));
    }

    @Override
    public boolean hset(int index, String key, String field, Object value) {
        return Boolean.TRUE.equals(baseRedisTemplate.execute((RedisConnection connection) -> {
            connection.select(index);
            RedisSerializer<String> serializer = baseRedisTemplate.getStringSerializer();
            String v = value instanceof String str ? str : toString(value);
            return connection.hashCommands().hSet(Objects.requireNonNull(serializer.serialize(key)), Objects.requireNonNull(serializer.serialize(field)), Objects.requireNonNull(serializer.serialize(v)));
        }));
    }

    @Override
    public boolean hmsetList(String key, Map<String, List<Object>> params) {
        if (CollectionUtil.isEmpty(params)) {
            return false;
        }
        Map<String, Object> operaterMap = new HashMap<>(params.size());


        operaterMap.putAll(params);
        return hmset(key, operaterMap);
    }

    @Override
    public boolean hmsetList(int index, String key, Map<String, List<Object>> params) {
        if (CollectionUtil.isEmpty(params)) {
            return false;
        }
        Map<String, Object> operaterMap = new HashMap<>(params.size());

        operaterMap.putAll(params);
        return hmset(key, operaterMap);
    }

    @Override
    public <T> boolean hmset(int index, String key, Map<String, T> params) {
        if (CollectionUtil.isEmpty(params)) {
            return false;
        }
        return Boolean.TRUE.equals(baseRedisTemplate.execute((RedisConnection connection) -> {
            connection.select(index);
            RedisSerializer<String> serializer = baseRedisTemplate.getStringSerializer();
            Map<byte[], byte[]> hmsetMap = new HashMap<>(params.size());
            params.forEach((field, value) -> {
                String val = value instanceof String str? str : toString(value);
                hmsetMap.put(
                        serializer.serialize(field),
                        serializer.serialize(val)
                );
            });
            connection.hashCommands().hMSet(Objects.requireNonNull(serializer.serialize(key)), hmsetMap);
            return true;
        }));
    }

    @Override
    public List<String> hmget(int index, String key, Collection<String> fields) {
        if (CollectionUtil.isEmpty(fields)) {
            return Collections.emptyList();
        }

        return baseRedisTemplate.execute((RedisConnection connection) -> {
            connection.select(index);
            RedisSerializer<String> serializer = baseRedisTemplate.getStringSerializer();

            List<byte[]> collector = fields.parallelStream().map(
                    serializer::serialize
            ).toList();

            List<byte[]> valueList = connection.hashCommands().hMGet(Objects.requireNonNull(serializer.serialize(key)), collector.toArray(byte[][]::new));
            if(CollectionUtil.isEmpty(valueList)){
                return Collections.emptyList();
            }
            return valueList.parallelStream().map(
                    serializer::deserialize
            ).collect(Collectors.toList());
        });
    }

    @Override
    public Map<String, String> hgetall(int index, String key) {
        return baseRedisTemplate.execute((RedisConnection connection) -> {
            connection.select(index);
            RedisSerializer<String> serializer = baseRedisTemplate.getStringSerializer();
            Map<byte[], byte[]> datas = connection.hashCommands().hGetAll(Objects.requireNonNull(serializer.serialize(key)));
            Map<String, String> result = new HashMap<>(Objects.requireNonNull(datas).size());
            datas.forEach((field, value) -> result.put(
                    serializer.deserialize(field),
                    serializer.deserialize(value)
            ));
            return result;
        });
    }

    @Override
    public List<String> hvals(String key) {
        return hvals(defaultDbIndex, key);
    }

    @Override
    public List<String> hkeys(String key) {
        return hkeys(defaultDbIndex, key);
    }

    @Override
    public List<String> hkeys(int index, String key) {
        return baseRedisTemplate.execute((RedisConnection connection) -> {
            connection.select(index);
            RedisSerializer<String> serializer = baseRedisTemplate.getStringSerializer();

            Set<byte[]> valueList = connection.hashCommands().hKeys(Objects.requireNonNull(serializer.serialize(key)));

            if(CollectionUtil.isEmpty(valueList)){
                return Collections.emptyList();
            }
            return valueList.parallelStream().map(
                    serializer::deserialize
            ).collect(Collectors.toList());
        });
    }

    @Override
    public List<String> hvals(int index, String key) {
        return baseRedisTemplate.execute((RedisConnection connection) -> {
            connection.select(index);
            RedisSerializer<String> serializer = baseRedisTemplate.getStringSerializer();
            List<byte[]> datas = connection.hashCommands().hVals(Objects.requireNonNull(serializer.serialize(key)));
            if(CollectionUtil.isEmpty(datas)){
                return Collections.emptyList();
            }
            return datas.parallelStream().map(serializer::deserialize).collect(Collectors.toList());
        });
    }

    @Override
    public boolean set(int index, String key, Object value) {
        return Boolean.TRUE.equals(baseRedisTemplate.execute((RedisConnection connection) -> {
            connection.select(index);
            RedisSerializer<String> serializer = baseRedisTemplate.getStringSerializer();
            connection.stringCommands().set(Objects.requireNonNull(serializer.serialize(key)), Objects.requireNonNull(serializer.serialize(value instanceof String str? str : toString(value))));
            return true;
        }));
    }

    @Override
    public Long incr(int index, String key) {
        return baseRedisTemplate.execute((RedisConnection connection) -> {
            connection.select(index);
            RedisSerializer<String> serializer = baseRedisTemplate.getStringSerializer();
            return connection.stringCommands().incr(Objects.requireNonNull(serializer.serialize(key)));
        });
    }

    @Override
    public boolean set(int index, String key, Object value, long expireTime) {
        return Boolean.TRUE.equals(baseRedisTemplate.execute((RedisConnection connection) -> {
            connection.select(index);
            RedisSerializer<String> serializer = baseRedisTemplate.getStringSerializer();
            if (value instanceof String str) {
                connection.stringCommands().setEx(Objects.requireNonNull(serializer.serialize(key)), expireTime, Objects.requireNonNull(serializer.serialize(str)));
            } else {
                connection.stringCommands().setEx(Objects.requireNonNull(serializer.serialize(key)), expireTime, Objects.requireNonNull(serializer.serialize(toString(value))));
            }
            return true;
        }));
    }

    @Override
    public String get(int index, String key) {
        return baseRedisTemplate.execute((RedisConnection connection) -> {
            connection.select(index);
            RedisSerializer<String> serializer = baseRedisTemplate.getStringSerializer();
            byte[] value = connection.stringCommands().get(Objects.requireNonNull(serializer.serialize(key)));
            return serializer.deserialize(value);
        });
    }

    @Override
    public boolean mset(int index, Map<String, Object> map) {
        if (CollectionUtil.isEmpty(map)) {
            return false;
        }
        return Boolean.TRUE.equals(baseRedisTemplate.execute((RedisConnection connection) -> {
            connection.select(index);
            RedisSerializer<String> serializer = baseRedisTemplate.getStringSerializer();
            Map<byte[], byte[]> result = new HashMap<>(map.size());
            map.forEach((key, value) -> result.put(
                    serializer.serialize(key),
                    serializer.serialize(value instanceof String str? str : toString(value))
            ));

            connection.stringCommands().mSet(result);
            return true;
        }));
    }

    @Override
    public Collection<String> mget(int index, Collection<String> keys) {
        if (CollectionUtil.isEmpty(keys)) {
            return Collections.emptyList();
        }
        return baseRedisTemplate.execute((RedisConnection connection) -> {
            connection.select(index);
            RedisSerializer<String> serializer = baseRedisTemplate.getStringSerializer();

            List<byte[]> collector = keys.parallelStream().map(serializer::serialize).collect(Collectors.toList());
            if(CollectionUtil.isEmpty(collector)){
                return Collections.emptyList();
            }
            collector = connection.stringCommands().mGet(collector.toArray(new byte[collector.size()][]));
            if(CollectionUtil.isEmpty(collector)){
                return Collections.emptyList();
            }
            return collector.parallelStream().map(
                    serializer::deserialize
            ).collect(Collectors.toList());
        });
    }

    @Override
    public Long append(String key, Object value) {
        return append(defaultDbIndex, key, value);
    }


    @Override
    public Long del(int index, String key) {
        return baseRedisTemplate.execute((RedisConnection connection) -> {
            connection.select(index);
            RedisSerializer<String> serializer = baseRedisTemplate.getStringSerializer();
            return connection.keyCommands().del(serializer.serialize(key));
        });
    }

    @Override
    public String hget(String key, String field) {
        return hget(defaultDbIndex, key, field);
    }

    @Override
    public String hget(int index, String key, String field) {
        return baseRedisTemplate.execute((RedisConnection connection) -> {
            connection.select(index);
            RedisSerializer<String> serializer = baseRedisTemplate.getStringSerializer();
            byte[] hGetResult = connection.hashCommands().hGet(Objects.requireNonNull(serializer.serialize(key)), Objects.requireNonNull(serializer.serialize(field)));
            return serializer.deserialize(hGetResult);
        });
    }

    @Override
    public Boolean expire(String key, long seconds) {
        return expire(defaultDbIndex, key, seconds);
    }


    @Override
    public Boolean expire(int index, String key, long seconds) {
        return baseRedisTemplate.execute((RedisConnection connection) -> {
            connection.select(index);
            RedisSerializer<String> serializer = baseRedisTemplate.getStringSerializer();
            return connection.keyCommands().expire(Objects.requireNonNull(serializer.serialize(key)), seconds);
        });
    }

    @Override
    public Long hincrby(String key, String field, Long increment) {
        return hincrby(defaultDbIndex, key, field, increment);
    }

    @Override
    public Long hincrby(int index, String key, String field, Long increment) {
        return baseRedisTemplate.execute((RedisConnection connection) -> {
            connection.select(index);
            RedisSerializer<String> serializer = baseRedisTemplate.getStringSerializer();
            return connection.hashCommands().hIncrBy(Objects.requireNonNull(serializer.serialize(key)), Objects.requireNonNull(serializer.serialize(field)), increment);
        });
    }

    @Override
    public Long hdel(int index, String key, Object field) {
        return baseRedisTemplate.execute((RedisConnection connection) -> {
            connection.select(index);
            RedisSerializer<String> serializer = baseRedisTemplate.getStringSerializer();
            if (field instanceof String f) {
                return connection.hashCommands().hDel(Objects.requireNonNull(serializer.serialize(key)), serializer.serialize(f));
            } else {
                @SuppressWarnings("unchecked")
                Collection<Object> val = (Collection<Object>) field;
                byte[][] f = (byte[][]) val.parallelStream().map(
                        value -> serializer.serialize((String) value)
                ).toArray();
                return connection.hashCommands().hDel(Objects.requireNonNull(serializer.serialize(key)), f);
            }
        });
    }

    @Override
    public Long hdel(String key, Object field) {
        return hdel(defaultDbIndex, key, field);
    }

    @Override
    public Boolean exists(String key) {
        return exists(defaultDbIndex, key);
    }

    @Override
    public Boolean exists(int index, String key) {
        return baseRedisTemplate.execute((RedisConnection connection) -> {
            connection.select(index);
            RedisSerializer<String> serializer = baseRedisTemplate.getStringSerializer();
            return connection.keyCommands().exists(Objects.requireNonNull(serializer.serialize(key)));
        });
    }


    @Override
    public Double zIncrBy(int index, String key, double score, Object member) {
        return baseRedisTemplate.execute((RedisConnection connection) -> {
            connection.select(index);
            RedisSerializer<String> serializer = baseRedisTemplate.getStringSerializer();
            String val = member instanceof String str ? str : toString(member);
            return connection.zSetCommands().zIncrBy(Objects.requireNonNull(serializer.serialize(key)), score, Objects.requireNonNull(serializer.serialize(val)));
        });
    }

    @Override
    public <T> List<T> zRevRange(int index, String key, long start, long end, Class<T> clz) {
        return baseRedisTemplate.execute((RedisConnection connection) -> {
            connection.select(index);
            RedisSerializer<String> serializer = baseRedisTemplate.getStringSerializer();
            Set<byte[]> bytes = connection.zSetCommands().zRevRange(Objects.requireNonNull(serializer.serialize(key)), start, end);
            List<T> list = new ArrayList<>();
            if (CollectionUtil.isNotEmpty(bytes)) {
                bytes.forEach(t -> list.add(JsonUtil.toData(serializer.deserialize(t), clz)));
            }
            return list;
        });
    }

    @Override
    public <T> Set<T> scan(int index, String pattern, int count, Class<T> clazz) {
        return baseRedisTemplate.execute((RedisConnection connection) -> {
            connection.select(index);
            RedisSerializer<String> serializer = baseRedisTemplate.getStringSerializer();
            Set<T> set = new HashSet<>();
            try (Cursor<byte[]> cursor = connection.keyCommands().scan(ScanOptions.scanOptions()
                    .match(pattern)
                    .count(count).build())) {
                while (cursor.hasNext()) {
                    set.add(JsonUtil.toData(serializer.deserialize(cursor.next()), clazz));
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            return set;
        });
    }
}
