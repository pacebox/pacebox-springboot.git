package tech.mhuang.pacebox.springboot.core.servlet;

import jakarta.servlet.ReadListener;
import jakarta.servlet.ServletInputStream;

import java.io.ByteArrayInputStream;

/**
 * input扩展支持重复读
 *
 * @author mhuang
 * @since 1.0.0
 */
public class WebServletInputStream extends ServletInputStream {

    private final ByteArrayInputStream in;

    public WebServletInputStream(byte[] body) {
        this.in = new ByteArrayInputStream(body);
    }

    @Override
    public int read() {
        return in.read();
    }

    @Override
    public boolean isFinished() {
        return false;
    }

    @Override
    public boolean isReady() {
        return false;
    }

    @Override
    public void setReadListener(ReadListener readListener) {

    }
}
