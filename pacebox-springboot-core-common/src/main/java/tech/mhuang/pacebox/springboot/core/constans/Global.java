package tech.mhuang.pacebox.springboot.core.constans;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 全局常量
 *
 * @author mhuang
 * @since 1.0.0
 */
public class Global {

    public static final String TYPE = "type";
    public static final String AUTH_TYPE = "authType";
    public static final String LANGUAGE = "language";
    public static final String SOURCE = "source";
    public static final String TENANT_ID = "tenantId";
    public static final String VERSION = "version";
    public static final String GLOBAL_HEADER = "global_header";
    public static final String USER_ID = "userId";
    public static final String COMPANY_ID = "companyId";

    public static final Set<String> IGNORE_HEADER = Stream.of(
            SOURCE,VERSION,TENANT_ID,LANGUAGE,
            TYPE,AUTH_TYPE,GLOBAL_HEADER,USER_ID,COMPANY_ID
    ).collect(Collectors.toSet());
    /**
     * 1分钟
     */
    public static final Long EXPIRE_ONE_MINUTES = 60L;
    /**
     * 5分钟过期
     */
    public static final Long EXPIRE_FIVE_MINUTES = 60 * 5L;

    /**
     * 10分钟过期
     */
    public static final Long EXPIRE_TEN_MINUTES = 60 * 10L;
    /**
     * 15分钟过期
     */
    public static final Long EXPIRE_FIFTEEN_MINUTES = 60 * 15L;
    /**
     * 30分钟过期
     */
    public static final Long EXPIRE_THIRTY_MINUTES = 60 * 30L;
    /**
     * 1小时过期
     */
    public static final Long EXPIRE_ONE_HOURS = 60 * 60L;
    /**
     * 2小时过期
     */
    public static final Long EXPIRE_TWO_HOURS = 60 * 60 * 2L;
    /**
     * 1天过期
     */
    public static final Long EXPIRE_ONE_DAY = 60 * 60 * 24L;
    /**
     * 1周过期
     */
    public static final Long EXPIRE_ONE_WEEKS = 60 * 60 * 24 * 7L;
    /**
     * 30天过期
     */
    public static final Long EXPIRE_THIRTY_DAYS = 60 * 60 * 24 * 30L;


}
