package tech.mhuang.pacebox.springboot.core.util;

import org.springframework.expression.EvaluationContext;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import tech.mhuang.pacebox.core.dict.BasicDict;

import java.util.Map;

/**
 * spring表达式解析
 *
 * @author mhuang
 * @since 2021.0.5.0
 */
public class SpelExpressionUtil {

    private final ExpressionParser parser = new SpelExpressionParser();

    /**
     * 获取解析实例
     *
     * @return 解析实例
     */
    public static SpelExpressionUtil getInstance() {
        return new SpelExpressionUtil();
    }

    /**
     * 获取值
     *
     * @param template 模板
     * @param params   参数
     * @param clazz    应答的数据
     * @param <T>      应答的数据类型
     * @return 结果集
     */
    public <T> T getValue(String template, BasicDict params, Class<T> clazz) {
        EvaluationContext context = new StandardEvaluationContext();
        for (Map.Entry<String, Object> entry : params.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            context.setVariable(key, value);
        }
        return parser.parseExpression(template).getValue(context, clazz);
    }
}