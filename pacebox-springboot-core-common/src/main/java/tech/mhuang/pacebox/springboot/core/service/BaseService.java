package tech.mhuang.pacebox.springboot.core.service;

import tech.mhuang.pacebox.springboot.protocol.InsertInto;
import tech.mhuang.pacebox.springboot.protocol.data.Page;

import java.io.Serializable;
import java.util.List;

/**
 * 通用service层
 *
 * @author mhuang
 * @since 1.0.0
 */
public interface BaseService<T extends Serializable, Id> {

    /**
     * 获取单个实例
     *
     * @param t 查询的实例对象
     * @return T 实例
     */
    T get(T t);

    /**
     * 根据Id获取单个实例
     *
     * @param id 查询的主键id
     * @return T 实例
     */
    T getById(Id id);


    /**
     * 插入
     *
     * @param t 插入集合
     * @return 添加个数
     */
    int insert(T t);

    /**
     * 修改单个实例
     *
     * @param t 实例对象
     * @return int 修改成功的个数
     */
    int update(T t);

    /**
     * 修改单个实例所有的字段
     *
     * @param t 修改的实例
     * @return 修改成功的个数
     */
    int updateAll(T t);

    /**
     * 删除
     *
     * @param id 删除的id
     * @return 删除个数
     */
    int delete(Id id);

    /**
     * 查询条数
     *
     * @param t 查询的实例
     * @return int 查询的个数
     */
    int count(T t);

    /**
     * 分页查询
     *
     * @param page 分页条件
     * @return 结果集
     */
    List<T> page(Page<T> page);

    /**
     * 总数查询
     *
     * @param page 条件
     * @return 结果
     */
    int pageCount(Page<T> page);

    /**
     * 查询全部
     *
     * @return 结果集
     */
    List<T> queryAll();

    /**
     * 条件查询
     *
     * @param t 条件
     * @return 结果集
     */
    List<T> query(T t);

    /**
     * 保存扩展
     *
     * @param into 扩展
     * @return 保存个数
     */
    int insertInto(InsertInto<Id> into);
}
