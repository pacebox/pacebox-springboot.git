package tech.mhuang.pacebox.springboot.core.validate;

import tech.mhuang.pacebox.springboot.core.util.IdCardUtil;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

/**
 * 身份证检查
 *
 * @author mhuang
 * @since 1.0.0
 */
public class IdCardCaseValidator implements ConstraintValidator<IdCardCase, String> {

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        return IdCardUtil.isValidatedAllIdcard(s);
    }
}
