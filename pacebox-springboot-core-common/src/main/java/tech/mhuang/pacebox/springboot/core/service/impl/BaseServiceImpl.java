package tech.mhuang.pacebox.springboot.core.service.impl;

import tech.mhuang.pacebox.springboot.core.mapper.BaseMapper;
import tech.mhuang.pacebox.springboot.core.service.BaseService;
import tech.mhuang.pacebox.springboot.protocol.InsertInto;
import tech.mhuang.pacebox.springboot.protocol.data.Page;

import java.io.Serializable;
import java.util.List;

/**
 * 通用service
 *
 * @author mhuang
 * @since 1.0.0
 */
public abstract class BaseServiceImpl<T extends Serializable, Id> implements BaseService<T, Id> {

    private  BaseMapper<T, Id> baseMapper;

    public void setBaseMapper(BaseMapper<T, Id> baseMapper) {
        this.baseMapper = baseMapper;
    }

    /**
     * 获取单个
     *
     * @param t 查询的实例对象
     * @return T
     */
    @Override
    public T get(T t) {
        return baseMapper.get(t);
    }

    /**
     * @param id 查询的主键id
     * @return T
     */
    @Override
    public T getById(Id id) {
        return baseMapper.getById(id);
    }

    /**
     * @param t 实例对象
     * @return 修改个数
     */
    @Override
    public int update(T t) {
        return baseMapper.update(t);
    }

    @Override
    public int updateAll(T t) {
        return baseMapper.updateAll(t);
    }

    /**
     * 查询实例的总数
     *
     * @param t 查询实例
     * @return 查询的个数
     */
    @Override
    public int count(T t) {
        return baseMapper.count(t);
    }

    @Override
    public int insert(T t) {
        return baseMapper.insert(t);
    }

    @Override
    public List<T> page(Page<T> page) {
        return baseMapper.page(page);
    }

    @Override
    public int pageCount(Page<T> page) {
        return baseMapper.pageCount(page);
    }

    @Override
    public List<T> queryAll() {
        return baseMapper.queryAll();
    }

    @Override
    public List<T> query(T t) {
        return baseMapper.query(t);
    }

    @Override
    public int delete(Id id) {
        return baseMapper.delete(id);
    }

    @Override
    public int insertInto(InsertInto<Id> into) {
        return baseMapper.insertInto(into);
    }
}
