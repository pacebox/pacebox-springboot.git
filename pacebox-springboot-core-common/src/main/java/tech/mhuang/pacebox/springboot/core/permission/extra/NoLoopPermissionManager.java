package tech.mhuang.pacebox.springboot.core.permission.extra;

import tech.mhuang.pacebox.springboot.core.permission.annotation.Permission;

public class NoLoopPermissionManager implements IPermissionManager{

    @Override
    public void permission(Permission permissionUser) {

    }
}
