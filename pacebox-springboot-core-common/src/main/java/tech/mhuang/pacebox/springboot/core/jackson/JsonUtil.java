package tech.mhuang.pacebox.springboot.core.jackson;

import tech.mhuang.pacebox.core.dict.BasicDict;
import tech.mhuang.pacebox.json.BaseJsonService;
import tech.mhuang.pacebox.json.jackson.JacksonJsonService;

import java.util.List;
import java.util.Map;

public class JsonUtil {

    private static BaseJsonService jsonConverter = new JacksonJsonService();

    public static void changeJson(BaseJsonService jsonService){
        jsonConverter = jsonService;
    }

    public static <T> String toString(T data) {
        return jsonConverter.toString(data);
    }

    public static <T> Map toMap(T data) {
        return jsonConverter.toMap(data);
    }

    public static <T> BasicDict toDict(T data) {
        return jsonConverter.toDict(data);
    }

    public static <T> T toData(String data, Class<T> clazz) {
        return jsonConverter.toData(data,clazz);
    }

    public static <T> List<T> toList(String data, Class<T> clazz) {
        return jsonConverter.toList(data,clazz);
    }

    public static <T> List<Map> toListMap(List<T> data) {
        return jsonConverter.toListMap(data);
    }
}
