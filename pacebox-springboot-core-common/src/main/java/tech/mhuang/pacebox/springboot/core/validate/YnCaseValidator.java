package tech.mhuang.pacebox.springboot.core.validate;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.List;

/**
 * 有效检查方法
 *
 * @author mhuang
 * @since 1.0.0
 */

public class YnCaseValidator implements ConstraintValidator<YnCase, String> {

    private List<String> compare;

    @Override
    public void initialize(YnCase constraintAnnotation) {
        compare = Arrays.asList(constraintAnnotation.value());
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return compare.contains(value);
    }
}
