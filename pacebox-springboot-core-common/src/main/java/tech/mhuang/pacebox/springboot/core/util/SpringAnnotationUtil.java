package tech.mhuang.pacebox.springboot.core.util;

import org.aopalliance.intercept.MethodInvocation;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.MethodParameter;
import tech.mhuang.pacebox.core.annotation.AnnotationUtil;

import java.lang.annotation.Annotation;

public class SpringAnnotationUtil extends AnnotationUtil{

    public static <T extends Annotation> T getAnnotation(MethodInvocation methodInvocation, Class<T> clazz) {
        return AnnotationUtil.getAnnotation(methodInvocation.getMethod(), clazz);
    }

    public static <T extends Annotation> T getAnnotation(MethodParameter methodParameter, Class<T> clazz) {
        return AnnotationUtil.getAnnotation(methodParameter.getMethod(), clazz);
    }

    public static <T extends Annotation> T getAnnotation(JoinPoint joinPoint, Class<T> clazz) {
        return getAnnotation((MethodSignature)joinPoint.getSignature(), clazz);
    }

    public static <T extends Annotation> T getAnnotation(MethodSignature methodSignature, Class<T> clazz) {
        return AnnotationUtil.getAnnotation(methodSignature.getMethod(), clazz);
    }
}
