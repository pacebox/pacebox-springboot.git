package tech.mhuang.pacebox.springboot.core.rest;

import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.util.ArrayList;
import java.util.List;

/**
 * 扩展mapper。支持text_plain
 *
 * @author mhuang
 * @since 1.0.0
 */
public class ExtractMappingJackson2HttpMessageConverter extends MappingJackson2HttpMessageConverter {
    public ExtractMappingJackson2HttpMessageConverter() {
        List<MediaType> mediaTypes = new ArrayList<>();
        mediaTypes.add(MediaType.TEXT_PLAIN);
        setSupportedMediaTypes(mediaTypes);
    }
}
