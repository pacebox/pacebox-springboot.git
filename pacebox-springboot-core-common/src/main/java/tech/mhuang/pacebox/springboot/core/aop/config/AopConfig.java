package tech.mhuang.pacebox.springboot.core.aop.config;

import lombok.Data;

/**
 * @author yuanhang.huang
 * @since 1.0.0
 */
@Data
public class AopConfig {

    public String name;

    public String advice;

    public String expression;

}
