package tech.mhuang.pacebox.springboot.core.okhttp;

import javax.net.ssl.X509TrustManager;
import java.security.cert.X509Certificate;

/**
 * 证书工具类
 *
 * @author mhuang
 * @since 1.0.0
 */
public class TrustAnyTrustManager implements X509TrustManager {
    public TrustAnyTrustManager() {
    }

    @Override
    public X509Certificate[] getAcceptedIssuers() {
        return new X509Certificate[0];
    }

    @Override
    public void checkClientTrusted(X509Certificate[] chain, String authType) {
    }

    @Override
    public void checkServerTrusted(X509Certificate[] chain, String authType) {
    }
}
