package tech.mhuang.pacebox.springboot.core.protocol.page;

import tech.mhuang.pacebox.springboot.core.spring.util.SpringServletUtil;
import tech.mhuang.pacebox.springboot.protocol.data.Page;
import tech.mhuang.pacebox.springboot.protocol.data.PageDTO;

/**
 * 分页工具类
 *
 * @author mhuang
 * @since 1.0.0
 */
public class PageUtil {

    private final static String START = "start";
    private final static String ROWS = "rows";

    /**
     * 检查分页满不满足条件
     *
     * @param totalCount 当前总数
     * @param pageDTO    分页条件
     * @return true满足 false不满足
     */
    public static boolean checkPage(Integer totalCount, PageDTO pageDTO) {
        boolean result = false;
        if (totalCount > 0) {
            result = totalCount >= ((pageDTO.getStart() - 1) * pageDTO.getRows()) + 1;
        }
        return result;
    }

    /**
     * 获取分页条件
     *
     * @return 分页
     */
    public static Page buildPage() {
        Page page = new Page();
        page.setStart(SpringServletUtil.getParameterAsType(START, Integer.class));
        page.setRows(SpringServletUtil.getParameterAsType(ROWS, Integer.class));
        return page;
    }
}
