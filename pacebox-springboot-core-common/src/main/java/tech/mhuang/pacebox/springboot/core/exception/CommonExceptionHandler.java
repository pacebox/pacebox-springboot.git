package tech.mhuang.pacebox.springboot.core.exception;

import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.BindException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import tech.mhuang.pacebox.core.exception.BusinessException;
import tech.mhuang.pacebox.core.exception.ExceptionUtil;
import tech.mhuang.pacebox.springboot.protocol.Result;


/**
 * 通用异常拦截
 *
 * @author mhuang
 * @since 1.0.0
 */
@ControllerAdvice
public class CommonExceptionHandler {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Value("${pacebox.exception.debug:false}")
    private Boolean debug;

    @Value("${system.error.message:服务器异常,请联系管理员}")
    private String systemErrorMessage;

    @Value("${not.support.error.message:不支持的请求方式}")
    private String notSupportedMessage;

    @ResponseBody
    @ExceptionHandler(value = Exception.class)
    public <T> Result<T>  defaultErrorHandler(HttpServletRequest request, Exception e) {

        logger.error("---Exception Handler---Host {} invokes url {} ERROR: {}",
                request.getRemoteHost(), request.getRequestURL(), e.getMessage(), e);

        Result<T> result = new Result<>();
        int code = Result.SYS_FAILD;
        String message, field = null;
        switch (e) {
            case BusinessException business -> {
                code = business.getCode();
                message = business.getMessage();
            }
            case BindException bindException -> {
                field = bindException.getAllErrors().getFirst().getCode();
                message = bindException.getAllErrors().getFirst().getDefaultMessage();
            }
            case HttpRequestMethodNotSupportedException ignored ->
                    message = notSupportedMessage + request.getMethod();
            default -> message = systemErrorMessage;
        }
        result.setField(field);
        result.setCode(code);
        result.setMessage(message);
        if (debug) {
            result.setExceptionMsg(ExceptionUtil.getMessage(e));
        }

        return result;
    }
}
