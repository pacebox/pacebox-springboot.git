package tech.mhuang.pacebox.springboot.core.aop.advice;

import org.springframework.aop.MethodBeforeAdvice;

/**
 * 前置通知接口
 *
 * @author yuanhang.huang
 * @since 1.0.0
 */
public interface BeforeAdvice extends MethodBeforeAdvice {
}
