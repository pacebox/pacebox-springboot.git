package tech.mhuang.pacebox.springboot.core.permission.extra;


import tech.mhuang.pacebox.springboot.core.permission.annotation.Permission;

/**
 * 权限
 *
 * @author mhuang
 * @since 1.0.0
 */
public interface IPermissionManager {
    /**
     * 执行应答结果
     *
     * @param permissionUser 权限参数
     */
    void permission(Permission permissionUser);
}
