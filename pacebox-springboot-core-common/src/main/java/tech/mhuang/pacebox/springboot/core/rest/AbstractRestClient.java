package tech.mhuang.pacebox.springboot.core.rest;

import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestClient;

public abstract class AbstractRestClient implements RestClient {

    private final RestClient restClient;

    protected AbstractRestClient(RestClient restClient) {
        this.restClient = restClient;
    }

    @Override
    public RequestHeadersUriSpec<?> get() {
        return restClient.get();
    }

    @Override
    public RequestHeadersUriSpec<?> head() {
        return restClient.head();
    }

    @Override
    public RequestBodyUriSpec post() {
        return restClient.post();
    }

    @Override
    public RequestBodyUriSpec put() {
        return restClient.put();
    }

    @Override
    public RequestBodyUriSpec patch() {
        return restClient.patch();
    }

    @Override
    public RequestHeadersUriSpec<?> delete() {
        return restClient.delete();
    }

    @Override
    public RequestHeadersUriSpec<?> options() {
        return restClient.options();
    }

    @Override
    public RequestBodyUriSpec method(HttpMethod method) {
        return restClient.method(method);
    }

    @Override
    public Builder mutate() {
        return restClient.mutate();
    }
}
