package tech.mhuang.pacebox.springboot.core.validate;

import tech.mhuang.pacebox.core.util.StringUtil;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

/**
 * 长度检查方法
 *
 * @author mhuang
 * @since 1.0.0
 */

public class LengthCaseValidator implements ConstraintValidator<LengthCase, String> {

    private int length;

    @Override
    public void initialize(LengthCase constraintAnnotation) {
        length = constraintAnnotation.length();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return length == StringUtil.length(value);
    }
}