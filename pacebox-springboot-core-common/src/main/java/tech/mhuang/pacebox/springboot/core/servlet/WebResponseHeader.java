package tech.mhuang.pacebox.springboot.core.servlet;

import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpServletResponseWrapper;

/**
 * 输出结果检测
 *
 * @author mhuang
 * @since 1.0.2
 */
public class WebResponseHeader extends HttpServletResponseWrapper implements HttpServletResponse {

    /**
     * Constructs a response adaptor wrapping the given response.
     *
     * @param response the {@link HttpServletResponse} to be wrapped.
     * @throws IllegalArgumentException if the response is null
     */
    public WebResponseHeader(HttpServletResponse response) {
        super(response);
    }
}
