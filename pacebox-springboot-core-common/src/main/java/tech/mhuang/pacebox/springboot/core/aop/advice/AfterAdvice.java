package tech.mhuang.pacebox.springboot.core.aop.advice;

import org.springframework.aop.AfterReturningAdvice;

/**
 * 后置通知接口
 *
 * @author yuanhang.huang
 * @since 1.0.0
 */
public interface AfterAdvice extends AfterReturningAdvice {

}
