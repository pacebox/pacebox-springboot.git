package tech.mhuang.pacebox.springboot.core.controller;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import tech.mhuang.pacebox.core.file.FileUtil;
import tech.mhuang.pacebox.springboot.protocol.Result;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * 通用控制层
 *
 * @author mhuang
 * @since 1.0.0
 */
public abstract class BaseController {

    /**
     * 页面跳转
     *
     * @param url 跳转的url
     * @return string
     */
    public String redirect(String url) {
        //节省开支
        return "redirect" + url;
    }

    /**
     * 成功应答
     *
     * @param data 数据
     * @param <T>  数据类型
     * @return result结果集
     * @since 2021.0.5.0
     */
    public <T> Result<T> success(T data) {
        return Result.ok(data);
    }

    /**
     * 错误状态应答
     *
     * @param message 错误消息
     * @return 错误结果集
     * @since 2021.0.5.0
     */
    public <T> Result<T> faild(String message) {
        return status(Result.SYS_FAILD, message);
    }


    /**
     * 错误消息应答
     *
     * @param code    错误码
     * @param message 错误结果集
     * @return 2021.0.5.0
     */
    public <T> Result<T> faild(int code, String message) {
        return status(code, message);
    }

    /**
     * 状态应答
     *
     * @param code 状态码
     * @param msg  消息
     * @return 结果集
     * @since 2021.0.5.0
     */
    public <T> Result<T> status(int code, String msg) {
        return Result.status(code, msg);
    }

    /**
     * 下载文件
     *
     * @param file 文件
     * @return 结果集
     * @throws IOException 异常
     * @since 2021.0.5.0
     */
    public ResponseEntity<byte[]> download(File file) throws IOException {
        return download(file, file.getName());
    }

    /**
     * 下载文件
     *
     * @param outputStream 文件流
     * @param fileName     文件名
     * @return 结果集
     * @since 2021.0.5.0
     */
    public ResponseEntity<byte[]> download(ByteArrayOutputStream outputStream, String fileName) {
        return download(outputStream.toByteArray(), fileName);
    }

    /**
     * 下载文件
     *
     * @param file     文件
     * @param fileName 文件名
     * @return 结果
     * @throws IOException 异常
     * @since 2021.0.5.0
     */
    public ResponseEntity<byte[]> download(File file, String fileName) throws IOException {
        return download(FileUtil.readFileToByteArray(file), fileName);
    }

    /**
     * 下载文件
     *
     * @param bytes    数据流
     * @param fileName 文件名
     * @return 结果
     * @since 2021.0.5.0
     */
    public ResponseEntity<byte[]> download(byte[] bytes, String fileName) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentDispositionFormData("attachment", new String(fileName.getBytes(), StandardCharsets.ISO_8859_1));
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        return new ResponseEntity<>(bytes, headers, HttpStatus.CREATED);

    }
}
