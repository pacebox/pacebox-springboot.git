package tech.mhuang.pacebox.springboot.core.rest;

import org.springframework.web.client.RestClient;

/**
 * 微服务RestTemplate
 *
 * @author mhuang
 * @since 2023.0.0.0
 */
public class MicroRestClient extends AbstractRestClient{

    public MicroRestClient(RestClient restClient){
        super(restClient);
    }
}
