package tech.mhuang.pacebox.springboot.core.permission.annotation;

import tech.mhuang.pacebox.springboot.core.permission.extra.IPermissionManager;
import tech.mhuang.pacebox.springboot.core.permission.extra.NoLoopPermissionManager;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 权限用户
 *
 * @author mhuang
 * @since 2021.0.5.0
 */
@Documented
@Retention(RUNTIME)
@Target(METHOD)
public @interface Permission {

    /**
     * 权限code
     *
     * @return 权限code
     */
    String code() default "";

    /**
     * 参数
     *
     * @return 参数
     */
    String param() default "";

    /**
     * 扩展参数
     *
     * @return 扩展参数
     */
    String extra() default "";


    /**
     * 权限处理器的bean,没有配置以manager为准，manager未配置则以通用配置为准，配置了以beanname配置，
     *
     * @return 权限处理器的bean
     */
    String beanName() default "";

    /**
     * 权限处理器的类，如果配置了采用了beanname，以beanname为准，此配置大于通用配置小于beanname配置
     *
     * @return 权限处理器的类
     */
    Class<? extends IPermissionManager> manager() default NoLoopPermissionManager.class;
}
