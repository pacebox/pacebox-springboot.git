package tech.mhuang.pacebox.springboot.core.rest;

import org.springframework.web.client.RestClient;
/**
 * 单服务RestTemplate
 *
 * @author mhuang
 * @since 2023.0.0.0
 */
public class SingleRestClient extends AbstractRestClient{

    public SingleRestClient(RestClient restClient){
        super(restClient);
    }
}
