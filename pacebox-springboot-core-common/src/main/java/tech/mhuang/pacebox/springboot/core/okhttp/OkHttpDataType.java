package tech.mhuang.pacebox.springboot.core.okhttp;

import okhttp3.MediaType;

/**
 * okhttpclient数据类型
 *
 * @author mhuang
 * @since 1.1.0
 */
public class OkHttpDataType {
    /**
     * HTML格式
     */
    public static final MediaType HTML_TYPE = MediaType.parse("text/html;charset=utf-8");

    /**
     * 纯文本格式
     */
    public static final MediaType PLAIN_TYPE = MediaType.parse("text/plain;charset=utf-8");

    /**
     * XML格式
     */
    public static final MediaType XML_TYPE = MediaType.parse("text/xml;charset=utf-8");

    /**
     * gif图片格式
     */
    public static final MediaType GIF_TYPE = MediaType.parse("image/gif");

    /**
     * jpg图片格式
     */
    public static final MediaType JPG_TYPE = MediaType.parse("image/jpeg");

    /**
     * png图片格式
     */
    public static MediaType PNG_TYPE = MediaType.parse("image/png");

    /**
     * XHTML格式
     */
    public static final MediaType XHTML_TYPE = MediaType.parse("application/xhtml+xml;charset=utf-8");

    /**
     * XML数据格式
     */
    public static final MediaType XML_DATA_TYPE = MediaType.parse("application/xml;charset=utf-8");

    /**
     * Atom XML聚合格式
     */
    public static final MediaType ATOM_XML_TYPE = MediaType.parse("application/atom+xml;charset=utf-8");

    /**
     * JSON数据格式
     */
    public static final MediaType JSON_TYPE = MediaType.parse("application/json;charset=utf-8");

    /**
     * pdf格式
     */
    public static final MediaType PDF_TYPE = MediaType.parse("application/pdf");

    /**
     * Word文档格式
     */
    public static final MediaType WORD_TYPE = MediaType.parse("application/msword");

    /**
     * 二进制流数据（如常见的文件下载）
     */
    public static final MediaType STREAM_TYPE = MediaType.parse("application/octet-stream");

    /**
     * form表单数据被编码为key/value格式发送到服务器
     */
    public static final MediaType ENC_TYPE = MediaType.parse("application/x-www-form-urlencoded");

    /**
     * 另外一种常见的媒体格式是上传文件之时使用的
     * 需要在表单中进行文件上传时，就需要使用该格式
     */
    public static final MediaType FORM_DATA_TYPE = MediaType.parse("multipart/form-data");
}
