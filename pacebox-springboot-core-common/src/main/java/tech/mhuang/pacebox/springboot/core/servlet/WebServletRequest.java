package tech.mhuang.pacebox.springboot.core.servlet;


import jakarta.servlet.http.HttpServletRequest;

/**
 * 扩展添加header接口
 *
 * @author mhuang
 * @since 1.0.0
 */
public interface WebServletRequest extends HttpServletRequest {

    /**
     * 设置请求头
     *
     * @param name  头名
     * @param value 头值
     */
    void putHeader(String name, String value);
}
