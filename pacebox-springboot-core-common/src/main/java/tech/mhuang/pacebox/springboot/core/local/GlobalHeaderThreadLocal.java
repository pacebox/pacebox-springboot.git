package tech.mhuang.pacebox.springboot.core.local;

import tech.mhuang.pacebox.core.exception.BusinessException;
import tech.mhuang.pacebox.core.util.StringUtil;
import tech.mhuang.pacebox.springboot.protocol.GlobalHeader;
import tech.mhuang.pacebox.springboot.protocol.Result;

/**
 * 全局当前线程的常量
 *
 * @author mhuang
 * @since 1.0.0
 */
public class GlobalHeaderThreadLocal {

    private static final ThreadLocal<GlobalHeader> globalUser = ThreadLocal.withInitial(() -> null);

    public static GlobalHeader get() {
        return globalUser.get();
    }

    public static GlobalHeader getOrException() {
        GlobalHeader globalHeader = get();
        if (globalHeader == null || StringUtil.isBlank(globalHeader.getUserId())) {
            throw new BusinessException(Result.TOKEN_EXPIRED, Result.TOKEN_IS_VALID_MSG);
        }
        return globalHeader;
    }

    public static void set(GlobalHeader value) {
        globalUser.set(value);
    }

    public static void remove() {
        globalUser.remove();
    }
}
