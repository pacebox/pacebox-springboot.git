package tech.mhuang.pacebox.springboot.core.rest;

import org.springframework.cglib.beans.BeanMap;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import tech.mhuang.pacebox.core.file.FileUtil;
import tech.mhuang.pacebox.core.charset.CharsetType;
import tech.mhuang.pacebox.core.util.CollectionUtil;
import tech.mhuang.pacebox.core.util.ObjectUtil;
import tech.mhuang.pacebox.core.util.StringUtil;
import tech.mhuang.pacebox.springboot.core.entity.RequestModel;
import tech.mhuang.pacebox.springboot.core.jackson.JsonUtil;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * RestTemplate抽象实现
 *
 * @author mhuang
 * @since 1.0.0
 */
public abstract class AbstractRestTemplate extends RestTemplate {

    public AbstractRestTemplate() {
        super();
    }

    public AbstractRestTemplate(ClientHttpRequestFactory requestFactory) {
        super(new BufferingClientHttpRequestFactory(requestFactory));
        List<HttpMessageConverter<?>> messageConverters = this.getMessageConverters();
        for (HttpMessageConverter<?> httpMessageConverter : messageConverters) {
            if (httpMessageConverter instanceof StringHttpMessageConverter stringHttpMessageConverter) {
                stringHttpMessageConverter.setDefaultCharset(CharsetType.UTF_8);
            }
        }
        this.getMessageConverters().add(new ExtractMappingJackson2HttpMessageConverter());
    }

    /**
     * 服务调用
     *
     * @param model 传递的参数
     * @param <T>   返回类型
     * @return ResponseEntity
     */
    public <T> ResponseEntity<T> request(RequestModel<T> model) {
        ResponseEntity<T> response = null;
        HttpMethod method = model.getMethod();
        String url = model.getUrl();
        String sufUrl = model.getSufUrl();
        Map<String, String> headerParamMap = model.getHeaderParamMap();
        Object params = model.getParams();
        HttpHeaders headers = new HttpHeaders();
        headers.setAll(headerParamMap);
        if (StringUtil.isNotBlank(sufUrl)) {
            url = url.concat(sufUrl);
        }
        if (method.equals(HttpMethod.GET) || method.equals(HttpMethod.DELETE)) {
            MultiValueMap<String, String> value = new LinkedMultiValueMap<>();
            if (ObjectUtil.isNotEmpty(params)) {
                Map<String,Object> tempData;
                if (params instanceof Map map) {
                    tempData = map;
                } else {
                    tempData = BeanMap.create(params);
                }
                if (CollectionUtil.isNotEmpty(tempData)) {
                    tempData.forEach((k, v) -> {
                        String tempValue = v instanceof String || ObjectUtil.isEmpty(v) ? (String) v : JsonUtil.toString(v);
                        value.add(k, tempValue);
                    });
                }
            }
            url = UriComponentsBuilder.fromHttpUrl(url).queryParams(value).build().toUriString();
            HttpEntity<T> entity = new HttpEntity<>(headers);
            response = exchange(url, method, entity, model.getTypeReference());
        } else if (method == HttpMethod.PUT || method == HttpMethod.POST) {
            headers.setContentType(model.getMediaType());
            HttpEntity<Object> request = new HttpEntity<>(params, headers);
            response = exchange(url, method, request, model.getTypeReference());
        }
        return response;
    }

    /**
     * 将file转换成byteArrayResource
     *
     * @param file File
     * @return ByteArrayResource资源
     * @throws IOException 完整异常
     */
    public ByteArrayResource convertFileToByteArrayResource(File file) throws IOException {

        return new ByteArrayResource(FileUtil.readFileToByteArray(file)) {
            @Override
            public String getFilename() {
                return file.getName();
            }
        };
    }

}
