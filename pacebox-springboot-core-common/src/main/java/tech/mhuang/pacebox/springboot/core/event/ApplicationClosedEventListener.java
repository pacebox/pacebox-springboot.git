package tech.mhuang.pacebox.springboot.core.event;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;

/**
 * spring停止运行
 *
 * @author mhuang
 * @since 2022.0.0.2
 */
@Slf4j
public class ApplicationClosedEventListener implements ApplicationListener<ContextClosedEvent> {
    @Override
    public void onApplicationEvent(ContextClosedEvent event) {
        log.debug("{} service stop", event.getApplicationContext().getId());
    }
}
