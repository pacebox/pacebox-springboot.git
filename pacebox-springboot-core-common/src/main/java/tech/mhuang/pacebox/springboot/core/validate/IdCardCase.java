package tech.mhuang.pacebox.springboot.core.validate;


import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * 检查身份证
 *
 * @author mhuang
 * @since 1.0.0
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(
        validatedBy = {IdCardCaseValidator.class}
)
public @interface IdCardCase {
    String message() default "{pacebox.validation.IdCardCase.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
