package tech.mhuang.pacebox.springboot.core.servlet;

import org.springframework.web.util.ContentCachingResponseWrapper;

import jakarta.servlet.http.HttpServletResponse;

/**
 * 输出结果检测
 *
 * @author mhuang
 * @since 1.0.2
 */
public class WebResponseStreamHeader extends ContentCachingResponseWrapper implements HttpServletResponse {

    /**
     * Constructs a response adaptor wrapping the given response.
     *
     * @param response the {@link HttpServletResponse} to be wrapped.
     * @throws IllegalArgumentException if the response is null
     */
    public WebResponseStreamHeader(HttpServletResponse response) {
        super(response);
    }
}
