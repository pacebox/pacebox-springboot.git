package tech.mhuang.pacebox.springboot.core.spring.util;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import tech.mhuang.pacebox.core.convert.Converter;
import tech.mhuang.pacebox.core.exception.BusinessException;
import tech.mhuang.pacebox.core.io.IOUtil;
import tech.mhuang.pacebox.core.sugar.Attempt;
import tech.mhuang.pacebox.core.charset.CharsetType;
import tech.mhuang.pacebox.springboot.protocol.Result;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import java.net.URLEncoder;

/**
 * spring工具类
 *
 * @author mhuang
 * @since 1.0.0
 */
public class SpringServletUtil {

    /**
     * 获取RequestAttributes
     *
     * @return ServletRequestAttributes
     */
    public static ServletRequestAttributes getRequestAttributes() {
        return (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
    }

    /**
     * 获取HttpServletRequest
     *
     * @return HttpServletRequest
     */
    public static HttpServletRequest getRequest() {
        return getRequestAttributes().getRequest();
    }

    /**
     * 获取请求参数值
     *
     * @param name 参数
     * @return 值
     */
    public static String getParameter(String name) {
        return getRequest().getParameter(name);
    }

    /**
     * 获取请求参数值，不存在则默认
     *
     * @param name         参数
     * @param defaultValue 默认值
     * @return 最终值
     */
    public static String getParameter(String name, String defaultValue) {
        return Converter.str(getParameter(name), defaultValue);
    }

    /**
     * 获取请求参数值带转换数据类型
     *
     * @param name  参数
     * @param clazz 参数值类型
     * @param <T>   参数值泛型
     * @return 参数值
     */
    public static <T> T getParameterAsType(String name, Class<T> clazz) {
        return Converter.convert(clazz, getParameter(name), null);
    }

    /**
     * 获取session
     *
     * @return HttpSession
     */
    public static HttpSession getSession() {
        return getRequest().getSession();
    }

    /**
     * 获取HttpServletResponse
     *
     * @return HttpServletResponse
     */
    public static HttpServletResponse getResponse() {
        return getRequestAttributes().getResponse();
    }

    /**
     * 应答数据
     *
     * @param text 数据
     */
    public static void write(String text) {
        write(getResponse(), text);
    }

    /**
     * 应答数据
     *
     * @param response HttpServletResponse
     * @param text     数据
     */
    public static void write(HttpServletResponse response, String text) {
        write(response, text, MediaType.APPLICATION_JSON_VALUE);
    }

    /**
     * 应答数据
     *
     * @param response    HttpServletResponse
     * @param text        数据
     * @param contentType 类型
     */
    public static void write(HttpServletResponse response, String text, String contentType) {
        response.setContentType(contentType);
        try (Writer writer = response.getWriter()) {
            writer.write(text);
            writer.flush();
        } catch (IOException e) {
            throw new BusinessException(Result.SYS_FAILD, Result.FAILD_MSG);
        }
    }

    /**
     * 写附件
     *
     * @param filename 文件名
     * @param data     文件数据
     * @return 应答ResponseEntity
     */
    public static ResponseEntity<byte[]> writeAttachment(String filename, byte[] data) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentDispositionFormData("attachment", Attempt.supply(() -> URLEncoder.encode(filename, CharsetType.UTF_8)).get());
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        return new ResponseEntity<>(data, headers, HttpStatus.CREATED);
    }

    /**
     * 写附件
     *
     * @param response 响应体
     * @param filename 文件名
     * @param data     数据
     */
    public static void writeAttachment(HttpServletResponse response, String filename, byte[] data) {
        response.setHeader("Content-Disposition", "attachment;filename=" + Attempt.supply(() -> URLEncoder.encode(filename, CharsetType.UTF_8)).get());
        response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
        try (OutputStream outputStream = response.getOutputStream()) {
            IOUtil.write(data, outputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}