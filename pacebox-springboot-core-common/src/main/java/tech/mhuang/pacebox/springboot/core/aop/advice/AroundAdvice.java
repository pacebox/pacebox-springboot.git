package tech.mhuang.pacebox.springboot.core.aop.advice;

import org.aopalliance.intercept.MethodInterceptor;

/**
 * 环绕通知接口
 *
 * @author yuanhang.huang
 * @since 1.0.0
 */
public interface AroundAdvice extends MethodInterceptor {
}
