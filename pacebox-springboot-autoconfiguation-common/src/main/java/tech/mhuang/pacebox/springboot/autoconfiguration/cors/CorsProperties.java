package tech.mhuang.pacebox.springboot.autoconfiguration.cors;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import tech.mhuang.pacebox.springboot.autoconfiguration.ConfigConsts;

/**
 * cors properties
 *
 * @author mhuang
 * @since 2020.0.0.2
 */
@Data
@ConfigurationProperties(prefix = ConfigConsts.CORS)
public class CorsProperties {

    private boolean enable;

    private String mappings = "/**";

    private String[] allowedOriginPatterns = new String[]{"*"};

    private boolean allowCredentials = true;

    private String[] headers = new String[]{"*"};

    private String[] methods = new String[]{"*"};

    private long maxAge = 3600;
}
