package tech.mhuang.pacebox.springboot.autoconfiguration.trace.redis;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import tech.mhuang.pacebox.springboot.autoconfiguration.ConfigConsts;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * redis埋点
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
@ConfigurationProperties(prefix = ConfigConsts.TRACE_REDIS)
public class TraceRedisProperties {

    private boolean enable;

    private Set<String> ignoreCommand;

    private Set<String> defaultIgnoreCommand = Stream.of("PING", "HELLO", "INFO").collect(Collectors.toSet());
}
