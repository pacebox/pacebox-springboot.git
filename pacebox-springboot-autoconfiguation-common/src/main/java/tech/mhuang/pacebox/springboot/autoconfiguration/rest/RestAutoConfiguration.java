package tech.mhuang.pacebox.springboot.autoconfiguration.rest;

import lombok.AllArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestClient;
import org.springframework.web.client.RestTemplate;
import tech.mhuang.pacebox.springboot.autoconfiguration.ConfigConsts;
import tech.mhuang.pacebox.springboot.core.rest.MicroRestClient;
import tech.mhuang.pacebox.springboot.core.rest.MicroRestTemplate;
import tech.mhuang.pacebox.springboot.core.rest.SingleRestClient;
import tech.mhuang.pacebox.springboot.core.rest.SingleRestTemplate;

import java.net.InetSocketAddress;
import java.net.Proxy;

/**
 * Rest自动注入类
 *
 * @author mhuang
 * @since 1.0.0
 */
@Configuration
@ConditionalOnClass(RestTemplate.class)
public class RestAutoConfiguration {

    static SimpleClientHttpRequestFactory generatorRequestFactory(RestProperties properties) {
        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
        if (properties.isUseProxy()) {
            requestFactory.setProxy(new Proxy(properties.getProxyType(),
                    new InetSocketAddress(properties.getProxyHost(), properties.getProxyPort())
            ));
        }
        requestFactory.setReadTimeout(properties.getReadTimeOut());
        requestFactory.setConnectTimeout(properties.getConnectTimeOut());
        requestFactory.setChunkSize(properties.getChunkSize());
        return requestFactory;
    }
    @AllArgsConstructor
    @Configuration
    @ConditionalOnProperty(prefix = ConfigConsts.REST_SINGLE, name = ConfigConsts.ENABLE, havingValue = ConfigConsts.TRUE)
    @EnableConfigurationProperties(RestSingleProperties.class)
    @ConditionalOnBean(SingleRestTemplate.class)
    static class SingleRestClientConfiguration{

        private final SingleRestTemplate singleRestTemplate;

        @Bean
        SingleRestClient singleRestClient(){
            return new SingleRestClient(RestClient.builder(singleRestTemplate).build());
        }
    }

    @AllArgsConstructor
    @Configuration
    @ConditionalOnProperty(prefix = ConfigConsts.REST_MICRO, name = ConfigConsts.ENABLE, havingValue = ConfigConsts.TRUE)
    @EnableConfigurationProperties(RestSingleProperties.class)
    @ConditionalOnBean(MicroRestTemplate.class)
    static class MicroRestClientConfiguration{

        private final MicroRestTemplate microRestTemplate;

        @Bean
        MicroRestClient microRestClient(){
            return new MicroRestClient(RestClient.builder(microRestTemplate).build());
        }
    }
    @AllArgsConstructor
    @Configuration
    @ConditionalOnProperty(prefix = ConfigConsts.REST_SINGLE, name = ConfigConsts.ENABLE, havingValue = ConfigConsts.TRUE)
    @EnableConfigurationProperties(RestSingleProperties.class)
    static class SingeRestTemplateConfiguration {

        private final RestSingleProperties properties;

        @Bean
        SingleRestTemplate singleRestTemplate() {
            return new SingleRestTemplate(generatorRequestFactory(this.properties));
        }
    }

    @AllArgsConstructor
    @Configuration
    @ConditionalOnProperty(prefix = ConfigConsts.REST_MICRO, name = ConfigConsts.ENABLE, havingValue = ConfigConsts.TRUE)
    @EnableConfigurationProperties(RestMicroProperties.class)
    static class MicroRestTemplateConfiguration {

        private final RestMicroProperties properties;

        @LoadBalanced
        @Bean
        MicroRestTemplate microRestTemplate() {
            return new MicroRestTemplate(generatorRequestFactory(this.properties));
        }
    }
}
