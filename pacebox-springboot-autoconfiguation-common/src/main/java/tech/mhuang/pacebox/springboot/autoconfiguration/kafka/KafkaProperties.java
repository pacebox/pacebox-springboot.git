package tech.mhuang.pacebox.springboot.autoconfiguration.kafka;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.context.properties.ConfigurationProperties;
import tech.mhuang.pacebox.kafka.admin.bean.KafkaInfo;
import tech.mhuang.pacebox.springboot.autoconfiguration.ConfigConsts;

/**
 * kafka配置信息
 *
 * @author mhuang
 * @since 1.0.0
 */
@ConfigurationProperties(prefix = ConfigConsts.KAFKA)
@Data
@EqualsAndHashCode(callSuper = false)
public class KafkaProperties extends KafkaInfo {

    /**
     * open kafka property..
     */
    private boolean enable;
}
