package tech.mhuang.pacebox.springboot.autoconfiguration.trace;

import io.micrometer.observation.ObservationRegistry;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.server.observation.ServerRequestObservationContext;
import tech.mhuang.pacebox.core.util.CollectionUtil;
import tech.mhuang.pacebox.springboot.autoconfiguration.ConfigConsts;
import tech.mhuang.pacebox.springboot.autoconfiguration.trace.mybatis.TraceMybatisConfiguration;
import tech.mhuang.pacebox.springboot.autoconfiguration.trace.okhttp.TraceOkHttpConfiguration;
import tech.mhuang.pacebox.springboot.autoconfiguration.trace.redis.TraceRedisAutoConfiguration;
import tech.mhuang.pacebox.springboot.autoconfiguration.trace.rest.TraceRestTemplateConfiguration;
import tech.mhuang.pacebox.springboot.autoconfiguration.trace.sms.TraceSmsConfiguration;

import java.util.HashSet;
import java.util.Set;

/**
 * 自动Trace注入
 *
 * @author mhuang
 * @since 1.0.0
 */
@Configuration
@Import({TraceMybatisConfiguration.class, TraceRestTemplateConfiguration.class,
        TraceSmsConfiguration.class, TraceOkHttpConfiguration.class,
        TraceRedisAutoConfiguration.class})
@EnableConfigurationProperties(value = TraceProperties.class)
@ConditionalOnProperty(prefix = ConfigConsts.TRACE, name = ConfigConsts.ENABLE, havingValue = ConfigConsts.TRUE)
public class TraceAutoConfiguration {

    public TraceAutoConfiguration(ObservationRegistry observationRegistry, TraceProperties traceProperties) {

        Set<String> ignoreMethod = new HashSet<>();

        if(CollectionUtil.isNotEmpty(traceProperties.getDefaultIgnoreRequestMethod())){
            ignoreMethod.addAll(traceProperties.getDefaultIgnoreRequestMethod());
        }
        if(CollectionUtil.isNotEmpty(traceProperties.getIgnoreRequestMethod())){
            ignoreMethod.addAll(traceProperties.getIgnoreRequestMethod());
        }
        observationRegistry.observationConfig().observationPredicate((obName, context) -> {
            //ignore requests method
            if (context instanceof ServerRequestObservationContext observationContext) {
                String method = observationContext.getCarrier().getMethod();
                return !ignoreMethod.contains(method);
            }
            return true;
        });
    }
}