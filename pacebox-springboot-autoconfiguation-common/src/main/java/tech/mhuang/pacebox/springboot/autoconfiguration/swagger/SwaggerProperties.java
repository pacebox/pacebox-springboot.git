package tech.mhuang.pacebox.springboot.autoconfiguration.swagger;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.context.properties.ConfigurationProperties;
import tech.mhuang.pacebox.springboot.autoconfiguration.ConfigConsts;

/**
 * swagger properties
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ConfigurationProperties(prefix = ConfigConsts.SWAGGER)
public class SwaggerProperties {

    /**
     * open swagger2
     */
    private boolean enable;

    /**
     * swagger title
     */
    private String title;

    /**
     * swagger description
     */
    private String description;

    /**
     * swagger version
     */
    private String version;


    /**
     * license url
     */
    private String licenseUrl;
    private String licenseName;

    /**
     * terms of service url
     */
    private String termsOfServiceUrl = "";

}
