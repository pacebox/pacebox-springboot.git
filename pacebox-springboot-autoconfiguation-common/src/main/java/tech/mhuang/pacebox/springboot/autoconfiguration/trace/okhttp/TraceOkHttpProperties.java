package tech.mhuang.pacebox.springboot.autoconfiguration.trace.okhttp;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import tech.mhuang.pacebox.springboot.autoconfiguration.ConfigConsts;
import tech.mhuang.pacebox.springboot.core.okhttp.TrustAnyTrustManager;

import javax.net.ssl.X509TrustManager;
import java.net.Proxy;

/**
 * RestTemplate 配置
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
@ConfigurationProperties(prefix = ConfigConsts.TRACE_OKHTTP)
public class TraceOkHttpProperties {
    private boolean enable = Boolean.TRUE;
    private String skipPattern = "";
    private Proxy.Type type = Proxy.Type.HTTP;
    private String proxyHost;
    private Integer proxyPort = 0;
    private String certPath;
    private String certPass;
    private X509TrustManager trustManager = new TrustAnyTrustManager();
}