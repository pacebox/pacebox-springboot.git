package tech.mhuang.pacebox.springboot.autoconfiguration.auth;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import tech.mhuang.pacebox.core.check.CheckAssert;
import tech.mhuang.pacebox.springboot.auth.AuthFilter;
import tech.mhuang.pacebox.springboot.auth.interceptor.InterInterceptor;
import tech.mhuang.pacebox.springboot.auth.interceptor.InterceptorBean;
import tech.mhuang.pacebox.springboot.autoconfiguration.ConfigConsts;

/**
 * 权限自动注入
 *
 * @author mhuang
 * @since 1.0.0
 */
@Configuration
@ConditionalOnProperty(prefix = ConfigConsts.AUTH, name = ConfigConsts.ENABLE, havingValue = ConfigConsts.TRUE)
@EnableConfigurationProperties(value = {AuthProperties.class})
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
@Slf4j
public class AuthAutoConfiguration {

    private final AuthProperties properties;

    public AuthAutoConfiguration(AuthProperties properties) {
        CheckAssert.check(properties, "not found auth properties");
        this.properties = properties;
    }

    @Bean
    public FilterRegistrationBean<AuthFilter> authRegistrationBean() {
        AuthFilter authFilter = new AuthFilter();
        authFilter.setDefaultAuthTypeValue(properties.getFilterDefAuthType());
        FilterRegistrationBean<AuthFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(authFilter);
        registrationBean.setUrlPatterns(this.properties.getFilterIncludeUrl());
        registrationBean.setOrder(200);
        registrationBean.setName("AuthFilter");
        return registrationBean;
    }

    @Bean
    @ConditionalOnBean(name = "authRegistrationBean")
    @ConditionalOnMissingBean
    public InterInterceptor interInterceptor() {
        InterInterceptor interInterceptor = new InterInterceptor();
        InterceptorBean bean = new InterceptorBean();
        bean.setRedisDatabase(properties.getRedisDataBase());
        bean.setCheckUrl(properties.isCheckInterceptorUrl());
        bean.setExcludeUrls(properties.getInterceptorExcludeUrl());
        bean.setIncludeUrls(properties.getInterceptorIncludeUrl());
        interInterceptor.setBean(bean);
        return interInterceptor;
    }

}
