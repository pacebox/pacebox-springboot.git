package tech.mhuang.pacebox.springboot.autoconfiguration.trace.openfeign;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import tech.mhuang.pacebox.springboot.autoconfiguration.ConfigConsts;

/**
 * Feign 配置
 *
 * @author mhuang
 * @since 2021.0.0.4
 */
@Data
@ConfigurationProperties(prefix = ConfigConsts.TRACE_OPENFEIGN)
public class TraceOpenFeignProperties {
    private boolean enable = Boolean.TRUE;
    private String skipPattern = "";
}