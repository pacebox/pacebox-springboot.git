package tech.mhuang.pacebox.springboot.autoconfiguration.oss;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import tech.mhuang.pacebox.oss.OssFieldProperties;
import tech.mhuang.pacebox.springboot.autoconfiguration.ConfigConsts;

import java.util.HashMap;
import java.util.Map;

/**
 * OSS配置参数
 *
 * @author mhuang
 * @since 1.0.7
 */
@Data
@ConfigurationProperties(prefix = ConfigConsts.OSS)
public class OssProperties {

    private boolean enable;

    /**
     * 需要配置的bean
     */
    private Map<String, OssFieldProperties> beanMap = new HashMap<>();
}
