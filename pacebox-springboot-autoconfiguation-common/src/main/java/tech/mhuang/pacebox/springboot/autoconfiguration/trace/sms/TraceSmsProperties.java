package tech.mhuang.pacebox.springboot.autoconfiguration.trace.sms;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import tech.mhuang.pacebox.springboot.autoconfiguration.ConfigConsts;

/**
 * 短信埋点
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
@ConfigurationProperties(prefix = ConfigConsts.TRACE_SMS)
public class TraceSmsProperties {
    private boolean enable;
}
