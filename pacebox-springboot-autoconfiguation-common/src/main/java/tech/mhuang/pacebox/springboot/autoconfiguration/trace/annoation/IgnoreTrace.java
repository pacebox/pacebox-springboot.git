package tech.mhuang.pacebox.springboot.autoconfiguration.trace.annoation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 埋点忽略
 *
 * @author mhuang
 * @since 1.0.0
 */
@Target(value = {ElementType.METHOD, ElementType.TYPE, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface IgnoreTrace {

    boolean hide() default true;

    boolean reqBodyHide() default false;

    boolean respBodyHide() default false;
}