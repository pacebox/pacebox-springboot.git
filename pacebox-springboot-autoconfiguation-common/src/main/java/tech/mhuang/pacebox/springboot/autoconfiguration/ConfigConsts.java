package tech.mhuang.pacebox.springboot.autoconfiguration;

/**
 * 配置常量
 *
 * @author mhuang
 * @since 1.0.0
 */
public class ConfigConsts {
    public static final String PROP = "pacebox.";
    public static final String SPRINGBOOT = PROP + "springboot";
    public static final String AUTH = PROP + "auth";
    public static final String REST = PROP + "rest";
    public static final String REST_SINGLE = REST + ".single";
    public static final String REST_MICRO = REST + ".micro";
    public static final String DATA_SECURE = PROP + "data-secure";
    public static final String ELASTICSEARCH = PROP + "elasticsearch";
    public static final String JWT = PROP + "jwt";
    public static final String KAFKA = PROP + "kafka";
    public static final String KAFKA_POOL = KAFKA + ".pool";
    public static final String REDIS = PROP + "redis";
    public static final String SWAGGER = PROP + "swagger";
    public static final String TASK = PROP + "task";
    public static final String WECHAT = PROP + "wechat";
    public static final String WECHAT_POOL = WECHAT + ".pool";
    public static final String SMS = PROP + "sms";
    public static final String OSS = PROP + "oss";

    public static final String ENABLE = "enable";
    public static final String TRACE = PROP + "trace";
    public static final String TRACE_SMS = TRACE + ".sms";
    public static final String TRACE_REDIS = TRACE + ".redis";
    public static final String TRACE_MYBATIS = TRACE + ".mybatis";
    public static final String TRACE_REST = TRACE + ".rest";
    public static final String TRACE_SERVLET = TRACE + ".servlet";
    public static final String TRACE_OKHTTP = TRACE + ".okhttp";
    public static final String TRACE_OPENFEIGN = TRACE + ".openfeign";

    public static final String EXCEPTION = PROP + "exception";
    public static final String VALIDATION = PROP + "validation";

    public static final String TRUE = "true";
    public static final String CORS = PROP + "cors";
    public static final String CROSS = PROP + "cross";
    public static final String PERMISSION = PROP + "permission";
    public static final String STOP_ENABLE = "stopEnable";
}
