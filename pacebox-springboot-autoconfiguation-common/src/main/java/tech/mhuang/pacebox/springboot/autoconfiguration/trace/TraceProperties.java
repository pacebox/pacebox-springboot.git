package tech.mhuang.pacebox.springboot.autoconfiguration.trace;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import tech.mhuang.pacebox.springboot.autoconfiguration.ConfigConsts;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 自动Trace配置
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
@ConfigurationProperties(prefix = ConfigConsts.TRACE)
public class TraceProperties {
    private boolean enable;

    private Set<String> ignoreRequestMethod;

    private Set<String> defaultIgnoreRequestMethod = Stream.of("OPTIONS").collect(Collectors.toSet());
}
