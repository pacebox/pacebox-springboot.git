package tech.mhuang.pacebox.springboot.autoconfiguration.permission;

import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.aspectj.AspectJExpressionPointcutAdvisor;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import tech.mhuang.pacebox.core.check.CheckAssert;
import tech.mhuang.pacebox.core.exception.BusinessException;
import tech.mhuang.pacebox.springboot.autoconfiguration.ConfigConsts;
import tech.mhuang.pacebox.springboot.autoconfiguration.SpringBootExtAutoConfiguration;
import tech.mhuang.pacebox.springboot.core.permission.PermissionInteceptor;
import tech.mhuang.pacebox.springboot.core.permission.extra.IPermissionManager;
import tech.mhuang.pacebox.springboot.core.spring.start.SpringContextHolder;

/**
 * springboot 权限通用工具
 *
 * @author mhuang
 * @since 2021.0.5.0
 */
@Configuration
@ConditionalOnClass(IPermissionManager.class)
@ConditionalOnProperty(prefix = ConfigConsts.PERMISSION, name = ConfigConsts.ENABLE, havingValue = ConfigConsts.TRUE)
@EnableConfigurationProperties(PermissionProperties.class)
@AutoConfigureAfter({SpringBootExtAutoConfiguration.class})
@Slf4j
public class PermissionAutoConfiguration {

    private final PermissionProperties permissionProperties;

    public PermissionAutoConfiguration(PermissionProperties permissionProperties) {
        this.permissionProperties = permissionProperties;
    }

    @Bean
    @ConditionalOnMissingBean
    public AspectJExpressionPointcutAdvisor permissionAdvisor(SpringContextHolder springContextHolder) {
        CheckAssert.check(springContextHolder, "SpringContextHolder不存在、请设置mhuang.holder.enable=true");
        IPermissionManager permissionManager = null;
        if (permissionProperties.getPermissionClass() != null) {
            try {
                permissionManager = permissionProperties.getPermissionClass().getDeclaredConstructor().newInstance();
            } catch (Exception e) {
                throw new BusinessException(-1, "系统初始化失败,配置的类无法实例化");
            }
        }
        if (permissionProperties.getBeanName() != null) {
            permissionManager = SpringContextHolder.getBean(permissionProperties.getBeanName(), IPermissionManager.class);
        }
        if(permissionManager == null){
            log.warn("permission未配置默认权限处理器,调用权限时会出现异常");
        }
        AspectJExpressionPointcutAdvisor advisor = new AspectJExpressionPointcutAdvisor();
        advisor.setAdvice(new PermissionInteceptor(permissionManager));
        advisor.setExpression(permissionProperties.getExpression());
        return advisor;
    }
}
