package tech.mhuang.pacebox.springboot.autoconfiguration.datasecure.annation;

import tech.mhuang.pacebox.springboot.autoconfiguration.datasecure.consts.EncryptType;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 加密（注解可在方法/类中使用）
 *
 * @author mhuang
 * @since 1.0.0
 */
@Target(value = {ElementType.METHOD, ElementType.TYPE, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface EncryptMapping {

    /**
     * 默认使用全局配置
     *
     * @return 配置key
     */
    String value() default "";

    /**
     * 默认整体加密
     *
     * @return 配置加密的方式
     */
    EncryptType type() default EncryptType.ALL;
}
