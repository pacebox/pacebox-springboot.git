package tech.mhuang.pacebox.springboot.autoconfiguration.elasticsearch;

import tech.mhuang.pacebox.elasticsearch.admin.external.IESExternal;
import tech.mhuang.pacebox.elasticsearch.admin.factory.IESFactory;
import tech.mhuang.pacebox.elasticsearch.server.ESFactory;
import tech.mhuang.pacebox.springboot.core.spring.start.SpringContextHolder;

/**
 * spring es扩展
 *
 * @author mhuang
 * @since 1.0.0
 */
public class SpringESExternal implements IESExternal {

    @Override
    public IESFactory create(String key) {
        return SpringContextHolder.registerBean(key, ESFactory.class);
    }
}
