package tech.mhuang.pacebox.springboot.autoconfiguration.datasecure.consts;

/**
 * 加密类型
 *
 * @author mhuang
 * @since 1.0.0
 */
public enum EncryptType {
    /**
     * 字段
     */
    FIELD,
    /**
     * 全部
     */
    ALL
}
