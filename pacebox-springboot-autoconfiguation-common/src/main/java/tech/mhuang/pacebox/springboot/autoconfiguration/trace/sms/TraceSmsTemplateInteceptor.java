package tech.mhuang.pacebox.springboot.autoconfiguration.trace.sms;

import io.micrometer.tracing.Span;
import io.micrometer.tracing.Tracer;
import lombok.extern.slf4j.Slf4j;
import tech.mhuang.pacebox.core.chain.BaseChain;
import tech.mhuang.pacebox.core.util.ObjectUtil;
import tech.mhuang.pacebox.sms.domain.SmsSendRequest;
import tech.mhuang.pacebox.sms.domain.SmsSendResult;
import tech.mhuang.pacebox.sms.inteceptor.SmsSendInterceptor;
import tech.mhuang.pacebox.springboot.core.jackson.JsonUtil;

/**
 * 短信埋点
 *
 * @author mhuang
 * @since 1.0.0
 */
@Slf4j
public class TraceSmsTemplateInteceptor implements SmsSendInterceptor {

    private final Tracer tracer;

    public TraceSmsTemplateInteceptor(Tracer tracer) {
        this.tracer = tracer;
    }

    @Override
    public SmsSendResult interceptor(BaseChain<SmsSendRequest, SmsSendResult> chain) {
        SmsSendResult result;
        SmsSendRequest request = chain.request();
        Span span = tracer.nextSpan().name("sms").tag("request.body", JsonUtil.toString(request));
        try (Tracer.SpanInScope ws = this.tracer.withSpan(span.start())) {
            result = chain.proceed(request);
            if (ObjectUtil.isEmpty(result.getThrowable())) {
                span.tag("response.body", JsonUtil.toString(result));
            } else {
                span.error(result.getThrowable());
            }
            span.event("smsEnd");
        } finally {
            span.end();
        }
        return result;
    }
}
