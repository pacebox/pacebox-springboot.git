package tech.mhuang.pacebox.springboot.autoconfiguration.datasecure;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import tech.mhuang.pacebox.springboot.autoconfiguration.ConfigConsts;
import tech.mhuang.pacebox.springboot.autoconfiguration.datasecure.advice.DataSecureRequestBodyAdvice;
import tech.mhuang.pacebox.springboot.autoconfiguration.datasecure.advice.DataSecureResponseBodyAdvice;

/**
 * 数据安全自动配置类
 *
 * @author mhuang
 * @since 1.0.0
 */
@Configuration
@ConditionalOnProperty(prefix = ConfigConsts.DATA_SECURE, name = ConfigConsts.ENABLE, havingValue = ConfigConsts.TRUE)
@EnableConfigurationProperties(DataSecureProperties.class)
public class DataSecureAutoConfiguration {

    /**
     * 请求解密
     *
     * @param properties 参数
     * @return 解密类
     */
    @Bean
    public DataSecureRequestBodyAdvice dataSecureRequestBodyAdvice(DataSecureProperties properties) {
        return new DataSecureRequestBodyAdvice(properties);
    }

    /**
     * 请求加密
     *
     * @param properties 参数
     * @return 加密类
     */
    @Bean
    public DataSecureResponseBodyAdvice dataSecureResponseBodyAdvice(DataSecureProperties properties) {
        return new DataSecureResponseBodyAdvice(properties);
    }
}
