package tech.mhuang.pacebox.springboot.autoconfiguration.rest;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.context.properties.ConfigurationProperties;
import tech.mhuang.pacebox.springboot.autoconfiguration.ConfigConsts;

/**
 * rest micro service call
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ConfigurationProperties(prefix = ConfigConsts.REST_MICRO)
public class RestMicroProperties extends RestProperties {

    /**
     * open micro enable default false
     */
    private boolean enable;
}
