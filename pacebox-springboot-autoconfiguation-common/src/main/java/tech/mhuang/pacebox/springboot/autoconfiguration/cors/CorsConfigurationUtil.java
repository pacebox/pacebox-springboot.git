package tech.mhuang.pacebox.springboot.autoconfiguration.cors;

import org.springframework.web.cors.CorsConfiguration;

import java.util.Arrays;

public class CorsConfigurationUtil {

    public static CorsConfiguration build(CorsProperties properties){
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        corsConfiguration.setAllowCredentials(properties.isAllowCredentials());
        corsConfiguration.setAllowedOriginPatterns(Arrays.asList(properties.getAllowedOriginPatterns()));
        corsConfiguration.setAllowedHeaders(Arrays.asList(properties.getHeaders()));
        corsConfiguration.setAllowedMethods(Arrays.asList(properties.getMethods()));
        corsConfiguration.setMaxAge(properties.getMaxAge());
        return corsConfiguration;
    }
}
