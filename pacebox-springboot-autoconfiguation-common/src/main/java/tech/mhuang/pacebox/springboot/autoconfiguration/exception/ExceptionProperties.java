package tech.mhuang.pacebox.springboot.autoconfiguration.exception;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import tech.mhuang.pacebox.springboot.autoconfiguration.ConfigConsts;

/**
 * SpringBoot扩展Exception配置
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
@ConfigurationProperties(prefix = ConfigConsts.EXCEPTION)
public class ExceptionProperties {

    private boolean enable;

    /**
     * 开启debug则会输出异常到前端
     */
    private boolean debug;
}