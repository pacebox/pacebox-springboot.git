package tech.mhuang.pacebox.springboot.autoconfiguration.trace.mybatis;

import io.micrometer.tracing.Tracer;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import tech.mhuang.pacebox.springboot.autoconfiguration.ConfigConsts;

/**
 * mybatis 配置
 *
 * @author mhuang
 * @since 1.0.0
 */
@Configuration
@EnableConfigurationProperties(value = TraceMybatisProperties.class)
@ConditionalOnProperty(prefix = ConfigConsts.TRACE_MYBATIS, name = ConfigConsts.ENABLE, havingValue = ConfigConsts.TRUE, matchIfMissing = true)
public class TraceMybatisConfiguration {
    public final Tracer tracer;

    public TraceMybatisConfiguration(Tracer tracer) {
        this.tracer = tracer;
    }

    @Bean
    public TraceMybatisInterceptor traceMybatisInterceptor() {
        return new TraceMybatisInterceptor(tracer);
    }
}