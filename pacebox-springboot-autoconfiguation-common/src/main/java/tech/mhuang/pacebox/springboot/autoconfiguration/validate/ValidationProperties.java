package tech.mhuang.pacebox.springboot.autoconfiguration.validate;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.context.properties.ConfigurationProperties;
import tech.mhuang.pacebox.core.charset.CharsetType;
import tech.mhuang.pacebox.springboot.autoconfiguration.ConfigConsts;

/**
 * 验证配置
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ConfigurationProperties(prefix = ConfigConsts.VALIDATION)
public class ValidationProperties {

    private boolean enable;

    private boolean alwaysUseMessageFormat = Boolean.FALSE;

    private boolean useCodeAsDefaultMessage = Boolean.FALSE;

    /**
     * 默认提供i18n和custom扩展
     */
    private String[] baseNames = {"i18n/message", "custom/message"};

    private String encoding = CharsetType.UTF_8_NAME;

    private int seconds = 3600;
}