package tech.mhuang.pacebox.springboot.autoconfiguration.permission;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import tech.mhuang.pacebox.springboot.autoconfiguration.ConfigConsts;
import tech.mhuang.pacebox.springboot.core.permission.extra.IPermissionManager;

/**
 * SpringBoot 权限属性
 *
 * @author mhuang
 * @since 2021.0.5.0
 */
@Data
@ConfigurationProperties(prefix = ConfigConsts.PERMISSION)
public class PermissionProperties {

    private boolean enable;

    /**
     * 权限表达式（默认是只处理permission注解）
     */
    private String expression = "@annotation(tech.mhuang.pacebox.springboot.core.permission.annotation.Permission)";

    /**
     * 通用执行的bean,如果没有指定则通过该方式获取
     */
    private String beanName;

    /**
     * 通用执行的类
     */
    private Class<? extends IPermissionManager> permissionClass;
}