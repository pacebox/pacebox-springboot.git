package tech.mhuang.pacebox.springboot.autoconfiguration.wechat;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import tech.mhuang.pacebox.springboot.autoconfiguration.ConfigConsts;
import tech.mhuang.pacebox.springboot.wechat.wechat.common.pool.ExecutorEventWechat;

/**
 * 微信配置
 *
 * @author mhuang
 * @since 1.0.0
 */
@Configuration
@ConditionalOnClass(ExecutorEventWechat.class)
@EnableConfigurationProperties({WechatProperties.class, WechatThreadPool.class})
@ConditionalOnProperty(prefix = ConfigConsts.WECHAT, name = ConfigConsts.ENABLE, havingValue = ConfigConsts.TRUE)
public class WechatAutoConfiguration {

    private final WechatThreadPool wechatThreadPool;

    public WechatAutoConfiguration(WechatProperties properties, WechatThreadPool wechatThreadPool) {
        this.wechatThreadPool = wechatThreadPool;
    }

    @Bean
    @ConditionalOnMissingBean
    public ExecutorEventWechat executorEventWechat() {
        ExecutorEventWechat executorEventWechat = new ExecutorEventWechat();
        wechatThreadPool.initialize();
        executorEventWechat.setEventService(wechatThreadPool);
        return executorEventWechat;
    }
}
