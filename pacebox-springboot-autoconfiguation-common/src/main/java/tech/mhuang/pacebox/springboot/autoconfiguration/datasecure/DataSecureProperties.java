package tech.mhuang.pacebox.springboot.autoconfiguration.datasecure;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.context.properties.ConfigurationProperties;
import tech.mhuang.pacebox.springboot.autoconfiguration.ConfigConsts;

import java.util.Map;

/**
 * 加解密配置
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ConfigurationProperties(prefix = ConfigConsts.DATA_SECURE)
public class DataSecureProperties extends DataSecureInfo {

    Map<String, DataSecureInfo> dataSecurePropertiesMap;
}
