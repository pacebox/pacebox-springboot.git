package tech.mhuang.pacebox.springboot.autoconfiguration.trace.redis;

import io.lettuce.core.RedisClient;
import io.lettuce.core.metrics.MicrometerCommandLatencyRecorder;
import io.lettuce.core.metrics.MicrometerOptions;
import io.lettuce.core.resource.ClientResources;
import io.lettuce.core.tracing.BraveTracing;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;
import tech.mhuang.pacebox.core.util.CollectionUtil;
import tech.mhuang.pacebox.springboot.autoconfiguration.ConfigConsts;

import java.util.HashSet;
import java.util.Set;

@Configuration
@ConditionalOnClass(RedisTemplate.class)
@AutoConfigureAfter(RedisAutoConfiguration.class)
@EnableConfigurationProperties(value = TraceRedisProperties.class)
@ConditionalOnProperty(prefix = ConfigConsts.TRACE_REDIS, name = ConfigConsts.ENABLE, havingValue = ConfigConsts.TRUE, matchIfMissing = true)
public class TraceRedisAutoConfiguration {

    private final TraceRedisProperties traceRedisProperties;

    public TraceRedisAutoConfiguration(TraceRedisProperties traceRedisProperties) {
        this.traceRedisProperties = traceRedisProperties;
    }

    @Bean
    public RedisClient redisClient(MeterRegistry meterRegistry) {
        MicrometerOptions options = MicrometerOptions.create();
        ClientResources resources = ClientResources.builder().commandLatencyRecorder(new MicrometerCommandLatencyRecorder(meterRegistry, options)).build();
        return RedisClient.create(resources);
    }

    @Bean
    public ClientResources clientResources(brave.Tracing clientTracing) {

        Set<String> ignoreCommand = new HashSet<>();

        if (CollectionUtil.isNotEmpty(traceRedisProperties.getDefaultIgnoreCommand())) {
            ignoreCommand.addAll(traceRedisProperties.getDefaultIgnoreCommand());
        }
        if (CollectionUtil.isNotEmpty(traceRedisProperties.getIgnoreCommand())) {
            ignoreCommand.addAll(traceRedisProperties.getIgnoreCommand());
        }
        BraveTracing tracing = BraveTracing.builder()
                .tracing(clientTracing)
//                .excludeCommandArgsFromSpanTags()
                .serviceName("lettuce-redis")
                .spanCustomizer((command, span) -> {
                    String commandName = command.getType().name();
                    if (ignoreCommand.contains(commandName)) {
                        //don`t report span
                        span.abandon();
                    } else {
                        span.tag("redis.cmd", commandName);
                        span.tag("redis.args", command.getArgs().toCommandString());
                    }
                })
                .build();
        return ClientResources.builder().tracing(tracing).build();
    }
}
