package tech.mhuang.pacebox.springboot.autoconfiguration.datasecure;

import lombok.Data;
import lombok.EqualsAndHashCode;
import tech.mhuang.pacebox.springboot.autoconfiguration.datasecure.impl.AESDataSecureImpl;

/**
 * 加解密配置
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class DataSecureInfo {

    /**
     * 是否启动加解密
     */
    private boolean enable;

    /**
     * 公钥（加密时使用）
     */
    private String publicKey;

    /**
     * 私钥（解密时使用）
     */
    private String privateKey;

    /**
     * 加密/解密用到的接口（默认是AES)
     */
    private Class<? extends AbstractDataSecure> encryptDataInterface = AESDataSecureImpl.class;
}