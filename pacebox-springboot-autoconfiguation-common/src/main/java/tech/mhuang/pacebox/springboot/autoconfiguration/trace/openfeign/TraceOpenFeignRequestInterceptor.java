package tech.mhuang.pacebox.springboot.autoconfiguration.trace.openfeign;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import io.micrometer.tracing.Tracer;

import java.util.regex.Pattern;

/**
 * feign 拦截器
 *
 * @author mhuang
 * @since 2021.0.0.4
 */
public class TraceOpenFeignRequestInterceptor implements RequestInterceptor {

    public TraceOpenFeignRequestInterceptor(Tracer tracer, Pattern compile) {
    }

    @Override
    public void apply(RequestTemplate requestTemplate) {
        //TODO 未完成的代码片段,此处埋点封装opentracing请求体
    }
}
