package tech.mhuang.pacebox.springboot.autoconfiguration.datasecure.annation;

import tech.mhuang.pacebox.springboot.autoconfiguration.datasecure.consts.DecryptType;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 解密（注解可在方法/类/参数中使用）
 *
 * @author mhuang
 * @since 1.0.0
 */
@Target(value = {ElementType.METHOD, ElementType.TYPE, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DecryptMapping {

    /**
     * 默认使用全局配置
     *
     * @return 配置key
     */
    String value() default "";

    /**
     * 默认整体解密
     *
     * @return 配置解密的方式
     */
    DecryptType type() default DecryptType.ALL;
}
