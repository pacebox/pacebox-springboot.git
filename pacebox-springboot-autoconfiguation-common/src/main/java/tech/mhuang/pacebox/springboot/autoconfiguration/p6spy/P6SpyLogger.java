package tech.mhuang.pacebox.springboot.autoconfiguration.p6spy;

import com.p6spy.engine.spy.appender.MessageFormattingStrategy;

import java.time.LocalDateTime;

/**
 * p6spy日志打印
 *
 * @author mhuang
 * @since 2021.0.5.0
 */
public class P6SpyLogger implements MessageFormattingStrategy {
    @Override
    public String formatMessage(int connectionId, String now, long elapsed, String category, String prepared, String sql, String url) {
        return !sql.trim().isEmpty() ? "[ " + LocalDateTime.now() + " ] --- | took "
                + elapsed + "ms | " + category + " | connection " + connectionId + "\n sql="
                + sql.replaceAll("\n", " ").replaceAll(" {2,}"," ") + ";" : "";
    }
}
