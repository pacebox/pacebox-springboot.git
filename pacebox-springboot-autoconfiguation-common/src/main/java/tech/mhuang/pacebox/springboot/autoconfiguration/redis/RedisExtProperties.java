package tech.mhuang.pacebox.springboot.autoconfiguration.redis;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import tech.mhuang.pacebox.springboot.autoconfiguration.ConfigConsts;

/**
 * redis扩展
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
@ConfigurationProperties(prefix = ConfigConsts.REDIS)
public class RedisExtProperties {

    private boolean enable;

}
