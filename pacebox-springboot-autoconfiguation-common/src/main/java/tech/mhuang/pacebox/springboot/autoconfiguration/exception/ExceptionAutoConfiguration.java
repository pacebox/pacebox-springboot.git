package tech.mhuang.pacebox.springboot.autoconfiguration.exception;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import tech.mhuang.pacebox.springboot.autoconfiguration.ConfigConsts;
import tech.mhuang.pacebox.springboot.core.exception.CommonExceptionHandler;

/**
 * 异常通用处理
 *
 * @author mhuang
 * @since 1.0.0
 */
@Configuration
@ConditionalOnClass(CommonExceptionHandler.class)
@EnableConfigurationProperties(value = ExceptionProperties.class)
@ConditionalOnProperty(prefix = ConfigConsts.EXCEPTION, name = ConfigConsts.ENABLE, havingValue = ConfigConsts.TRUE)
public class ExceptionAutoConfiguration {

    @Bean
    public CommonExceptionHandler commonExceptionHandler() {
        return new CommonExceptionHandler();
    }
}