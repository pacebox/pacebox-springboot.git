package tech.mhuang.pacebox.springboot.autoconfiguration.trace.openfeign;

import feign.Client;
import io.micrometer.tracing.Tracer;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import tech.mhuang.pacebox.springboot.autoconfiguration.ConfigConsts;

import java.util.regex.Pattern;

/**
 * trace feign
 *
 * @author mhuang
 * @since 2021.0.0.4
 */
@Configuration
@ConditionalOnClass(Client.class)
@AutoConfigureBefore(FeignAutoConfiguration.class)
@EnableConfigurationProperties(value = TraceOpenFeignProperties.class)
@ConditionalOnProperty(prefix = ConfigConsts.TRACE_OPENFEIGN, name = ConfigConsts.ENABLE, havingValue = ConfigConsts.TRUE, matchIfMissing = true)
public class TraceOpenFeignConfiguration {

    public final Tracer tracer;
    public final TraceOpenFeignProperties properties;

    public TraceOpenFeignConfiguration(Tracer tracer, TraceOpenFeignProperties properties) {
        this.tracer = tracer;
        this.properties = properties;
    }

    @Bean
    public TraceOpenFeignRequestInterceptor traceFeignRequestInterceptor() {
        return new TraceOpenFeignRequestInterceptor(tracer, Pattern.compile(properties.getSkipPattern()));
    }
}
