package tech.mhuang.pacebox.springboot.autoconfiguration.trace.rest;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import tech.mhuang.pacebox.springboot.autoconfiguration.ConfigConsts;

/**
 * RestTemplate 配置
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
@ConfigurationProperties(prefix = ConfigConsts.TRACE_REST)
public class TraceRestTemplateProperties {
    private boolean enable = Boolean.TRUE;
    private String skipPattern = "";
}