package tech.mhuang.pacebox.springboot.autoconfiguration.cors;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;
import tech.mhuang.pacebox.springboot.autoconfiguration.ConfigConsts;

import java.util.Arrays;

/**
 * reactor cors 配置
 *
 * @author mhuang
 * @since 1.0.0
 */
@Configuration
@EnableConfigurationProperties(value = CorsProperties.class)
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.REACTIVE)
@ConditionalOnProperty(prefix = ConfigConsts.CORS, name = ConfigConsts.ENABLE, havingValue = ConfigConsts.TRUE)
public class InterReactorCorsAutoConfiguration {

    private final CorsProperties properties;

    public InterReactorCorsAutoConfiguration(CorsProperties properties) {
        this.properties = properties;
    }

    @Bean
    @Order(Integer.MIN_VALUE)
    public CorsWebFilter corsWebFilter() {
        UrlBasedCorsConfigurationSource urlBasedCorsConfigurationSource = new UrlBasedCorsConfigurationSource();
        urlBasedCorsConfigurationSource.registerCorsConfiguration(properties.getMappings(), CorsConfigurationUtil.build(properties));
        return new CorsWebFilter(urlBasedCorsConfigurationSource);
    }
}
