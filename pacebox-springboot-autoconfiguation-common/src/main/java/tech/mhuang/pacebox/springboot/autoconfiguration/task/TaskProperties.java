package tech.mhuang.pacebox.springboot.autoconfiguration.task;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.util.ErrorHandler;
import tech.mhuang.pacebox.springboot.autoconfiguration.ConfigConsts;

/**
 * task properties
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ConfigurationProperties(prefix = ConfigConsts.TASK)
public class TaskProperties {

    /**
     * default task enable is <code>FALSE</code>
     */
    private boolean enable;

    /**
     * default name is <code>defaultInterTask</code>
     */
    private String name = "defaultInterTask";

    private Boolean removeOnCancelPolicy = Boolean.FALSE;

    private Boolean continueExistingPeriodicTasksAfterShutdownPolicy = Boolean.FALSE;

    private Boolean executeExistingDelayedTasksAfterShutdownPolicy = Boolean.TRUE;

    private Class<ErrorHandler> errorHandler;
    /**
     * default pool is <code>50</code>
     */
    private int poolSize = 50;
}
