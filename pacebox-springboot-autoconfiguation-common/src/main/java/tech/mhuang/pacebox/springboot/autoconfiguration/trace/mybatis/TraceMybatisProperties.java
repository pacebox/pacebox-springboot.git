package tech.mhuang.pacebox.springboot.autoconfiguration.trace.mybatis;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import tech.mhuang.pacebox.springboot.autoconfiguration.ConfigConsts;

/**
 * mybatis埋点
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
@ConfigurationProperties(prefix = ConfigConsts.TRACE_MYBATIS)
public class TraceMybatisProperties {
    private boolean enable = Boolean.TRUE;
}