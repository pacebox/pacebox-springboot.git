package tech.mhuang.pacebox.springboot.autoconfiguration.trace.openfeign;

import feign.Client;
import feign.Request;
import feign.Response;

import java.io.IOException;

/**
 * feign重构
 *
 * @author mhuang
 * @since 2021.0.0.4
 */
public class TraceFeignClient implements Client {

    private final Client delegate;

    public TraceFeignClient(Client delegate) {
        this.delegate = delegate;
    }

    @Override
    public Response execute(Request request, Request.Options options) throws IOException {
        return delegate.execute(request, options);
    }
}
