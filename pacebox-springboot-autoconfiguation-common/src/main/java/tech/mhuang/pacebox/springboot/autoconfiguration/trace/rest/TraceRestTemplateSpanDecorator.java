package tech.mhuang.pacebox.springboot.autoconfiguration.trace.rest;

import io.micrometer.tracing.Span;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpResponse;
import tech.mhuang.pacebox.core.io.IOUtil;

import java.io.IOException;


/**
 * restTemplate span埋点
 *
 * @author mhuang
 * @since 1.0.0
 */
@Slf4j
public class TraceRestTemplateSpanDecorator {


    public void onRequest(HttpRequest request, Span span) {
        span.tag("http.url", request.getURI().toString());
        span.tag("http.method", request.getMethod().toString());
        if (request.getURI().getPort() != -1) {
            span.tag("http.port", String.valueOf(request.getURI().getPort()));
        }
    }

    public void onResponse(ClientHttpResponse response, Span span) {
        try {
            span.tag("http.status", String.valueOf(response.getStatusCode().value()));
            String content = IOUtil.toString(response.getBody());
            span.tag("result", content);
        } catch (IOException e) {
            log.error("Could not get HTTP status code");
        }
    }

    public void onError(HttpRequest httpRequest, Throwable ex, Span span) {
        span.error(ex);
    }
}