package tech.mhuang.pacebox.springboot.autoconfiguration.wechat;

import org.springframework.boot.context.properties.ConfigurationProperties;
import tech.mhuang.pacebox.springboot.autoconfiguration.ConfigConsts;
import tech.mhuang.pacebox.springboot.core.spring.pool.SpringThreadPool;

/**
 * 微信线程池配置
 *
 * @author mhuang
 * @since 1.0.0
 */
@ConfigurationProperties(prefix = ConfigConsts.WECHAT_POOL)
public class WechatThreadPool extends SpringThreadPool {

    public WechatThreadPool() {
        super();
        String DEFAULT_NAME = "wechatThreadPool";
        setBeanName(DEFAULT_NAME);
    }
}